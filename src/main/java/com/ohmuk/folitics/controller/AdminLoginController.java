package com.ohmuk.folitics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.model.UserOutputModelFull;
import com.ohmuk.folitics.service.interfaces.UserAdminAuthenticateService;

@Controller
@RequestMapping("/admin")
public class AdminLoginController {
	private static Logger logger = LoggerFactory.getLogger(AdminLoginController.class);
	@Autowired
	private UserAdminAuthenticateService userAdminAuthenticateService;

	@RequestMapping(value = "/authenticateAdmin", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> authenticateAdmin(
			@RequestParam(value = "name", required = true) String userName,
			@RequestParam(value = "password", required = true) String password) {

		logger.info("Inside AdminController authenticateAdmin method");
		UserOutputModelFull user = null;
		try {
			user = UserOutputModelFull.getModel(userAdminAuthenticateService.authenticateUser(userName, password));
		} catch (Exception exception) {
			logger.error("Exception in user login with user id: " + userName);
			logger.error("Exception: " + exception);
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("User AuthienticateUser Success");
			return new ResponseDto<UserOutputModelFull>(true, user);
		}
		logger.debug("User AuthienticateUser Un-Successfully");
		return new ResponseDto<UserOutputModelFull>(false);
	}

}

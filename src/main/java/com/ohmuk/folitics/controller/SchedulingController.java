package com.ohmuk.folitics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.service.IGPIAggregationSchedular;
import com.ohmuk.folitics.util.SchedulerUtil;

/**
 * @author Kalpana
 *
 */

@Controller
@RequestMapping("/scheduler")
public class SchedulingController {

	@Autowired
	IGPIAggregationSchedular gpiAggregationSchedular;

	@RequestMapping(value = "/startAggregation", method = RequestMethod.GET)
	public @ResponseBody String startAggregation(String schedulerType) {
		if (schedulerType == null) {
			schedulerType = SchedulerUtil.ScheduleType.HOURLY;
		}
		String satus = gpiAggregationSchedular.startMonitoring(schedulerType);
		return satus;
	}

	@RequestMapping(value = "/stopAggregation", method = RequestMethod.GET)
	public @ResponseBody String stopAggregation(String schedulerType) {
		if (schedulerType == null) {
			schedulerType = SchedulerUtil.ScheduleType.HOURLY;
		}
		String satus = gpiAggregationSchedular.stopMonitoring(schedulerType);
		return satus;
	}
}
/**
 * 
 */
package com.ohmuk.folitics.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.ISourceRatingGraphBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.ouput.model.SourceRatingGraphOutputModel;

/**
 * @author Deewan
 *
 */
@Controller
@RequestMapping("/sourceratinggraph")
public class SourceRatingGraphController {
	private static Logger logger = Logger
			.getLogger(SourceRatingGraphController.class);
	@Autowired
	private volatile ISourceRatingGraphBusinessDelegate sourceRatingGraphBusinessDelegate;

	@RequestMapping(value = "/sourceRatingGraph", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<SourceRatingGraphOutputModel>> sourceRatingGraph(String feedName) {
		logger.info("Inside SourceRatingGraphController sourceRatingGraph method");
		List<SourceRatingGraphOutputModel> outputModelsForTop50Percentage = null;
		List<SourceRatingGraphOutputModel> outputModelsForRatingTypePercentage = null;
		List<SourceRatingGraphOutputModel> outputModelsForRatingTypeBarGraph = null;		
		try {
			System.out.println("Feed Name: "+feedName);
			outputModelsForTop50Percentage = sourceRatingGraphBusinessDelegate.top50PercentRatingType(feedName);
			outputModelsForRatingTypePercentage = sourceRatingGraphBusinessDelegate.ratingTypePercentage(feedName);
			outputModelsForRatingTypeBarGraph = sourceRatingGraphBusinessDelegate.ratingTypeBarGraph(feedName);
			
			if (outputModelsForTop50Percentage != null) {
				return new ResponseDto<List<SourceRatingGraphOutputModel>>(
						false, outputModelsForTop50Percentage);
			}
		} catch (Exception exception) {
			logger.error("Exception in sourceRatingGraph");
			logger.error("Exception: " + exception);
			logger.info("Exiting from SourceRatingGraphController sourceRatingGraph method");
			return new ResponseDto<List<SourceRatingGraphOutputModel>>(false);
		}
		return new ResponseDto<List<SourceRatingGraphOutputModel>>(false);
	}

}

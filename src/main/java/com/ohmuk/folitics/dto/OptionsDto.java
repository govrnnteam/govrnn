package com.ohmuk.folitics.dto;

import java.io.Serializable;

public class OptionsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String optionText;

	public String getOptionText() {
		return optionText;
	}

	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	@Override
	public String toString() {
		return "OptionsDto [optionText=" + optionText + "]";
	}
}

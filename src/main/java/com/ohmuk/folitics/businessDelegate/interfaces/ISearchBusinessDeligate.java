package com.ohmuk.folitics.businessDelegate.interfaces;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.elasticsearch.action.search.SearchResponse;

import com.ohmuk.folitics.businessDelegate.implementations.SearchOutputModel;

public interface ISearchBusinessDeligate {
	
	/**
     * Method is to search
     * 
     * @param index , type
     * @return SearchResponse
     * @throws Exception
     */
	public SearchResponse searchDocument(String index, String type, String text) throws UnknownHostException;

    public List<SearchOutputModel> searchByComponent(String searchKeyword, String componentType, Long userId, HttpServletRequest request) throws Exception;

    public String getBaseUrl(HttpServletRequest request) throws UnknownHostException, SocketException;

}

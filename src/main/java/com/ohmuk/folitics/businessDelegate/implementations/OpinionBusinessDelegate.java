package com.ohmuk.folitics.businessDelegate.implementations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.enums.OpinionType;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentOpinionStat;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.notification.InterfaceNotificationService;
import com.ohmuk.folitics.notification.NotificationMapping;
import com.ohmuk.folitics.ouput.model.OpinionOutputModel;
import com.ohmuk.folitics.ouput.model.PastOpinionOutputModel;
import com.ohmuk.folitics.ouput.model.UserOutputModel;
import com.ohmuk.folitics.service.ILinkService;
import com.ohmuk.folitics.service.IOpinionService;
import com.ohmuk.folitics.service.ISentimentOpinionStatService;
import com.ohmuk.folitics.service.ITrendService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.NotificationUtil;
import com.ohmuk.folitics.util.ThumbnailUtil;
import com.ohmuk.folitics.service.ISentimentService;
/**
 * @author Abhishek
 *
 */

@Component
public class OpinionBusinessDelegate implements IOpinionBusinessDelegate {

    private static Logger logger = LoggerFactory.getLogger(OpinionBusinessDelegate.class);

    @Autowired
    private volatile IOpinionService service;

    @Autowired
    private volatile ISentimentService sentimentService;

    @Autowired
    private volatile IUserService userService;

    @Autowired
    private volatile ISentimentOpinionStatService sentimentOpinionStatService;

    @Autowired
    private volatile InterfaceNotificationService notificationService;

    @Autowired
    private volatile ILinkService linkService;
    
    @Autowired
    private volatile ITrendService trendService;

    public static final String IMAGE_PATH = "/src/main/resources/static/images/sentiments/";

    public static final String IMAGE_PATH_1 = "/images/sentiments/";

    public static final String TEMPORARY_SENTIMENT = "." + File.separator + "testdata" + File.separator + "sentiment"
            + File.separator + "temporary" + "sentiment1.jpg";

    @Override
    public Opinion create(Opinion opinion) throws Exception {
        logger.info("Inside create method in business delegate");
        NotificationMapping notificationMapping = new NotificationMapping();
        /*
         * if (opinion.getLink() != null) { if (opinion.getLink().getSentiment()
         * == null) { opinion.getLink().setSentiment(sentiment); } else {
         * opinion.getLink().setSentiment(opinion.getSentiment()); } }
         * 
         * if(sentiment.getPolls() != null){
         * if(!sentiment.existsPoll(opinion.getAgreeDisagrePoll())){
         * sentiment.addPoll(opinion.getAgreeDisagrePoll()); } }else{
         * sentiment.addPoll(opinion.getAgreeDisagrePoll()); }
         */
        // if(opinion.getAgreeDisagrePoll().getSentiment() == null)
        // opinion.getAgreeDisagrePoll().setSentiment(sentiment);

        Sentiment sentiment = sentimentService.read(opinion.getSentiment().getId());
        opinion.setSentiment(sentiment);
        opinion.setUser(userService.findUserById(opinion.getUser().getId()));
        Link link = null;
        logger.info("Opinion Link : " + opinion.getLink());
        if (opinion.getLink() != null && !StringUtils.isEmpty(opinion.getLink().getLink())) {
            opinion.getLink().setComponentType(ComponentType.LINK.getValue());
            opinion.getLink().setCreated_By(opinion.getCreated_By());
            if (org.springframework.util.StringUtils.isEmpty(opinion.getLink().getDescription())) {
                opinion.getLink().setDescription(opinion.getLink().getLink());
            }
            link = linkService.create(opinion.getLink());
        }

        opinion.setLink(link);
        opinion = service.create(opinion);
        // sentimentService.update(sentiment);
        // call for notification
        if (opinion != null) {
            notificationMapping = NotificationUtil.prepareOpinionNotification(opinion);
            notificationService.opinionNotification(notificationMapping, opinion.getSentiment().getId());
        }
        if (opinion != null) {
            SentimentOpinionStat sentimentOpinionStat = sentimentOpinionStatService
                    .read(opinion.getSentiment().getId());
            if (sentimentOpinionStat == null) {
                sentimentOpinionStat = new SentimentOpinionStat();
                sentimentOpinionStat.setId(opinion.getSentiment().getId());
                long favorPoints = 0, againstPoints = 0;
                if (opinion.getType().equals(OpinionType.PROGOVT.getValue())) {
                    favorPoints = Constants.OPINION_WEIGHT;
                } else if (opinion.getType().equals(OpinionType.ANTIGOVT.getValue())) {
                    againstPoints = Constants.OPINION_WEIGHT;
                }
                sentimentOpinionStat.setFavorPoints(favorPoints);
                sentimentOpinionStat.setAgainstPoints(againstPoints);
                sentimentOpinionStatService.create(sentimentOpinionStat);
            } else {
                long favorPoints = 0, againstPoints = 0;
                favorPoints = sentimentOpinionStat.getFavorPoints();
                againstPoints = sentimentOpinionStat.getAgainstPoints();
                if (opinion.getType().equals(OpinionType.PROGOVT.getValue())) {
                    favorPoints = favorPoints + Constants.OPINION_WEIGHT;
                } else if (opinion.getType().equals(OpinionType.ANTIGOVT.getValue())) {
                    againstPoints = againstPoints + Constants.OPINION_WEIGHT;
                }
                sentimentOpinionStatService.update(sentimentOpinionStat);
            }
        }
        if (opinion.getUserIds() == null || opinion.getUserIds().size() < 0) {

        } else {
            List<Long> users = new ArrayList<Long>();
            logger.info("opinion .get UserID ::" + opinion.getUserIds());
            for (Long userId : opinion.getUserIds()) {
                logger.info("UserId : " + userId);
                if (!userId.equals(notificationMapping.getUserId())) {
                    users.add(userId);
                }
            }
            notificationService.sendTagNotification(notificationMapping, users);
        }

        logger.info("Exiting create method in business delegate");

        return opinion;
    }

    @Override
    public Opinion read(Long id) throws Exception {
        logger.info("Inside read method in business delegate");
        Opinion opinion = service.read(id);
        logger.info("Exiting read method in business delegate");
        return opinion;
    }

    @Override
    public List<Opinion> readAll() throws Exception {

        logger.info("Inside readAll method in business delegate");
        List<Opinion> opinions = service.readAll();
        logger.info("Exiting readAll method in business delegate");
        return opinions;
    }

    @Override
    public Opinion update(Opinion opinion) throws Exception {
        logger.info("Inside update method in business delegate");
        Opinion opinionData = service.update(opinion);
        logger.info("Exiting update method in business delegate");
        return opinionData;
    }

    @Override
    public Opinion delete(Long id) throws Exception {
        logger.info("Inside delete method in business delegate");
        Opinion opinion = service.read(id);
        opinion.setState(ComponentState.DELETED.getValue());
        Opinion opinionData = service.update(opinion);
        return opinionData;
    }

    @Override
    public Opinion delete(Opinion opinion) throws Exception {
        logger.info("Inside delete method in business delegate");
        opinion.setState(ComponentState.DELETED.getValue());
        Opinion opinionData = service.update(opinion);
        logger.info("Exiting delete method in business delegate");
        return opinionData;
    }

    @Override
    public boolean deleteFromDB(Long id) throws Exception {
        logger.info("Inside deleteFromDB method in business delegate");
        boolean sucess = service.deleteFromDBById(id);
        notificationService.deleteNotificationByComponentIdAndType(id, ComponentType.OPINION.getValue());
        notificationService.deleteNotificationByComponentIdAndType(id, ComponentType.RESPONSE.getValue());

        logger.info("Exiting deleteFromDB method in business delegate");
        return sucess;
    }

    @Override
    public boolean deleteFromDB(Opinion opinion) throws Exception {
        logger.info("Inside deleteFromDB method in business delegate");
        boolean sucess = service.deleteFromDB(opinion);
        logger.info("Exiting deleteFromDB method in business delegate");
        return sucess;
    }

    @Override
    public List<Opinion> getTopMostOpinion() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Opinion> getOpinionByUser(Long userId) throws MessageException, Exception {
        logger.info("Inside getOpinionByUser method in business delegate");
        List<Opinion> opinion = service.getOpinionByUser(userId);
        logger.info("Exiting getOpinionByUser method in business delegate");
        return opinion;
    }

    @Override
    public List<Opinion> getOpinionForSentiment(Long id) throws Exception {
        logger.info("Inside getOpinionForSentiment method in business delegate");
        List<Opinion> opinion = service.getOpinionForSentiment(id);
        logger.info("Exiting getOpinionForSentiment method in business delegate");
        return opinion;
    }

    @Override
    public List<Opinion> getOpinionForSentimentAndUser(Long sentimentId, Long userId) throws Exception {
        logger.info("Inside getOpinionForSentiment method in business delegate");
        List<Opinion> opinion = service.getOpinionForSentimentAndUser(sentimentId, userId);
        logger.info("Exiting getOpinionForSentiment method in business delegate");
        return opinion;
    }

    @Override
    public List<Opinion> getOpinionsByType(Long sentimentId, String opinionType, int count) throws Exception {
        logger.info("Inside getOpinionsByType method in business delegate");
        List<Opinion> opinion = service.getOpinionByType(sentimentId, opinionType, count);
        logger.info("Exiting getOpinionsByType method in business delegate");
        return opinion;
    }

    @Override
    public OpinionAggregation getOpinionsAggregate(Long sentimentId) throws Exception {
        logger.info("Inside getOpinionsAggregate method in business delegate");
        OpinionAggregation opinionAggregation = service.getOpinionsAggregate(sentimentId);
        logger.info("Exiting getOpinionsAggregate method in business delegate");
        return opinionAggregation;
    }

    @Override
    public List<Opinion> getOpinionComponent(Long sentimentId, Long componentId, String componentType) throws Exception {

        logger.info("Inside readAll method in business delegate");
        List<Opinion> opinions = service.getOpinionComponent(sentimentId, componentId, componentType);
        logger.info("Exiting readAll method in business delegate");
        return opinions;
    }

    @Override
    public @ResponseBody List<OpinionOutputModel> getLatestOpinions(int count) throws Exception {

        logger.info("Inside readAll method in business delegate");
        List<Opinion> opinions = service.getLatestOpinions(count);

        if (opinions != null && !opinions.isEmpty()) {
            List<OpinionOutputModel> models = new ArrayList<>();
            for (Opinion opinion : opinions) {
                OpinionOutputModel model = new OpinionOutputModel();
                Sentiment sentiment = opinion.getSentiment();
                model.setId(opinion.getId());
                model.setType(opinion.getType());
                model.setTitle(opinion.getSubject());
                model.setText(opinion.getText());
                if(opinion.getUser() != null){
                    model.setUser(UserOutputModel.getModel(opinion.getUser()));                    
                }
//                model.setImage((opinion.getAttachment() != null && opinion.getAttachment().getAttachmentFile() != null) ? opinion
//                        .getAttachment().getAttachmentFile().getData()
//                        : (sentiment != null ? sentiment.getImage() : null));
                model.setImageUrl(opinion.getImageUrl());
                if(StringUtils.isEmpty(opinion.getImageUrl())){
	                try {
	                    model.setImage(ThumbnailUtil.getSentimentImageThumbnail(sentiment.getImage(),
	                            sentiment.getImageType()));
	                    model.setImageFileType(sentiment.getImageType());
	                } catch (IOException e) {
	                    model.setImage(sentiment.getImage());
	                    model.setImageFileType(sentiment.getImageType());
	                    e.printStackTrace();
	                }
                }
                model.setSentimentName(sentiment != null ? sentiment.getSubject() : null);
                model.setSentimentId(sentiment != null ? sentiment.getId() : null);
                model.setSentimentCategory(sentiment != null ? sentiment.getType() : null);
                model.setFormattedAge(DateUtils.getDateOrTimeAgo(opinion.getCreateTime()));
                models.add(model);
            }
            logger.info("Exiting readAll method in business delegate");
            return models;
        }
        logger.info("Exiting readAll method in business delegate");
        return null;
    }

    @Override
    public LinkedHashSet<Opinion> searchOpinion(String searchKeyword, Long sentimentId) throws Exception {
        LinkedHashSet<Opinion> opinionSet = new LinkedHashSet<Opinion>();
        if (sentimentId == null) {
            opinionSet = service.searchOpinion(searchKeyword);
        } else {
            opinionSet = service.searchOpinionBySearchKeywordAndSentimentId(searchKeyword, sentimentId);

        }

        return opinionSet;
    }

    @Override
    public PastOpinionOutputModel getPastOpinionOutputModel(Long userId) throws Exception {

        logger.info("Inside getPastOpinionOutputModel method in business delegate");
        List<Opinion> opinions = service.readAll();
        PastOpinionOutputModel pastOpinionOutputModel = new PastOpinionOutputModel();
        Map<String, Integer> proMap = new HashMap<>();
        Map<String, Integer> antiMap = new HashMap<>();

        if (opinions != null && !opinions.isEmpty()) {

            for (Opinion opinion : opinions) {

                User user = opinion.getUser();
                if (user.getId().longValue() == userId.longValue()) {
                    Date date = new Date(opinion.getCreateTime().getTime());
                    String monthYear = (new SimpleDateFormat("MMM-yyyy")).format(date);
                    if (opinion.getType().equalsIgnoreCase(OpinionType.PROGOVT.getValue())) {
                        if (proMap != null) {
                            if (proMap.containsKey(monthYear)) {
                                proMap.put(monthYear, proMap.get(monthYear) + 1);
                            } else {
                                proMap.put(monthYear, 1);
                            }

                        }
                    } else {
                        if (antiMap != null) {
                            if (antiMap.containsKey(monthYear)) {
                                antiMap.put(monthYear, antiMap.get(monthYear) + 1);
                            } else {
                                antiMap.put(monthYear, 1);
                            }

                        }
                    }
                }
            }

        }

        if (proMap != null && !proMap.isEmpty()) {
            int totalPros = 0;
            List<PastOpinionOutputModel.OpinionByMonth> opinionByMonths = new ArrayList<>();
            int totalAnti = 0;
            for (Entry<String, Integer> entry : proMap.entrySet()) {

                PastOpinionOutputModel.OpinionByMonth opinionByMonth = pastOpinionOutputModel.new OpinionByMonth();

                opinionByMonth.setMonth(entry.getKey());
                opinionByMonth.setSupport(entry.getValue());
                if (antiMap.get(entry.getKey()) != null) {
                    opinionByMonth.setAgainst(antiMap.get(entry.getKey()));
                }
                totalPros = totalPros + opinionByMonth.getSupport();
                totalAnti = totalAnti + opinionByMonth.getAgainst();
                opinionByMonths.add(opinionByMonth);
            }

            pastOpinionOutputModel.setSupport((long) ((totalPros * 100) / (totalPros + totalAnti)));
            pastOpinionOutputModel.setAgainst((long) (100 - (pastOpinionOutputModel.getSupport())));
            pastOpinionOutputModel.setOpinionByMonths(opinionByMonths);
        }
        // only anti
        else{

            int totalPros = 0;
            List<PastOpinionOutputModel.OpinionByMonth> opinionByMonths = new ArrayList<>();
            int totalAnti = 0;
            for (Entry<String, Integer> entry : antiMap.entrySet()) {

                PastOpinionOutputModel.OpinionByMonth opinionByMonth = pastOpinionOutputModel.new OpinionByMonth();

                opinionByMonth.setMonth(entry.getKey());
                opinionByMonth.setAgainst(entry.getValue());
                if (antiMap.get(entry.getKey()) != null) {
                    opinionByMonth.setAgainst(antiMap.get(entry.getKey()));
                }
                totalPros = totalPros + opinionByMonth.getSupport();
                totalAnti = totalAnti + opinionByMonth.getAgainst();
                opinionByMonths.add(opinionByMonth);
            }

            pastOpinionOutputModel.setSupport((long) ((totalPros * 100) / (totalPros + totalAnti)));
            pastOpinionOutputModel.setAgainst((long) (100 - (pastOpinionOutputModel.getSupport())));
            pastOpinionOutputModel.setOpinionByMonths(opinionByMonths);
        
        	
        }
        logger.info("Exiting getPastOpinionOutputModel method in business delegate");
        return pastOpinionOutputModel;
    }

    @Override
    public String uploadOpinionImage(MultipartFile file, Long sentimentId, Long userId) throws Exception {

        Sentiment sentiment = sentimentService.read(sentimentId);
        String imagePath = "";

        if (sentiment != null) {
            String fileName = null;
            if (!file.isEmpty()) {
                try {
                    String ext = FilenameUtils.getExtension(file.getOriginalFilename());
                    logger.info("File extension : " + ext);
                    String[] extension = file.getOriginalFilename().split("\\.");
                    File convFile = new File(System.getProperty("user.dir") + IMAGE_PATH + sentiment.getSubject() + "/"
                            + "userImages/" + File.separator);

                    if (!convFile.exists() && !convFile.isDirectory()) {
                        convFile.mkdirs();
                    }
                    String renameImageName = "/" + extension[0] + "_" + userId + "_"
                            + new Timestamp(System.currentTimeMillis()) + "." + extension[1];
                    imagePath = convFile + renameImageName;
                    File f = new File(imagePath);

                    byte[] imagebyte = ThumbnailUtil.getSentimentImageThumbnail(file.getBytes(), ext);
                    FileOutputStream fileOuputStream = new FileOutputStream(f);

                    // FileUtils.writeByteArrayToFile(f, imagebyte);

                    fileOuputStream.write(imagebyte);
                    fileOuputStream.close();

                    imagePath = f.getAbsolutePath();
                    imagePath = IMAGE_PATH_1 + sentiment.getSubject() + "/" + "userImages" + renameImageName;

                    logger.info("Exiting uploadImage  method in business delegate");
                    return imagePath;

                } catch (Exception e) {
                    logger.error("You failed to upload " + fileName + ": " + e.getMessage(), e);
                }
            }

        }
        return imagePath;

    }


    @Override
    public @ResponseBody List<OpinionOutputModel> getRelatedOpinions(Long id) throws Exception {

        logger.info("Inside getRelatedOpinions method in business delegate");
        Opinion opinion1 = service.read(id);
        List<TrendMapping> opinionTrends = null;
        if(opinion1 != null){
        	opinionTrends = trendService.getTrendByComponetIdType(ComponentType.OPINION.getValue(), opinion1.getId());
        }
        List<Long> opinionTrendId = null;
        if(opinionTrends != null){
        	opinionTrendId = new ArrayList<>();
        	for(TrendMapping mapping : opinionTrends){
        		opinionTrendId.add(mapping.getId()); 
        	}
        }
        List<Opinion> opinions = service.getRelatedOpinions(id,opinion1.getSubject(),10,opinionTrendId);

        if (opinions != null && !opinions.isEmpty()) {
            List<OpinionOutputModel> models = new ArrayList<>();
            for (Opinion opinion : opinions) {
                OpinionOutputModel model = new OpinionOutputModel();
                Sentiment sentiment = opinion.getSentiment();
                model.setId(opinion.getId());
                model.setType(opinion.getType());
                model.setTitle(opinion.getSubject());
                model.setText(opinion.getText());
                if(opinion.getUser() != null){
                    model.setUser(UserOutputModel.getModel(opinion.getUser()));                    
                }// model.setUserId(opinion.getUser().getId());
                // model.setUserName(opinion.getUser().getName());
                model.setImage((opinion.getAttachment() != null && opinion.getAttachment().getAttachmentFile() != null) ? opinion
                        .getAttachment().getAttachmentFile().getData()
                        : (sentiment != null ? sentiment.getImage() : null));
                try {
                    model.setImage(ThumbnailUtil.getSentimentImageThumbnail(sentiment.getImage(),
                            sentiment.getImageType()));
                    model.setImageFileType(sentiment.getImageType());
                } catch (IOException e) {
                    model.setImage(sentiment.getImage());
                    model.setImageFileType(sentiment.getImageType());
                    e.printStackTrace();
                }

                model.setSentimentName(sentiment != null ? sentiment.getSubject() : null);
                model.setSentimentId(sentiment != null ? sentiment.getId() : null);
                model.setSentimentCategory(sentiment != null ? sentiment.getType() : null);
                model.setFormattedAge(DateUtils.getDateOrTimeAgo(opinion.getCreateTime()));
                models.add(model);
            }
            logger.info("Exiting getRelatedOpinions method in business delegate");
            return models;
        }
        logger.info("Exiting getRelatedOpinions method in business delegate");
        return null;
    }

}

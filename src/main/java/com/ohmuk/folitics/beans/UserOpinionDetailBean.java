package com.ohmuk.folitics.beans;

import java.sql.Timestamp;

public class UserOpinionDetailBean  {

	private Long sentimentId;
	private String description;
	private byte[] image;
	private String imageType;
	private String sentimentType;
	private Long opinionId;
	private String opinionImage;
	private String opinionSubject;
	private String opinionDescription;
	private String opinionType;
	private Timestamp createTime;

	public Long getSentimentId() {
		return sentimentId;
	}
	public void setSentimentId(Long sentimentId) {
		this.sentimentId = sentimentId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getSentimentType() {
		return sentimentType;
	}
	public void setSentimentType(String sentimentType) {
		this.sentimentType = sentimentType;
	}
	public Long getOpinionId() {
		return opinionId;
	}
	public void setOpinionId(Long opinionId) {
		this.opinionId = opinionId;
	}
	public String getOpinionImage() {
		return opinionImage;
	}
	public void setOpinionImage(String opinionImage) {
		this.opinionImage = opinionImage;
	}
	public String getOpinionSubject() {
		return opinionSubject;
	}
	public void setOpinionSubject(String opinionSubject) {
		this.opinionSubject = opinionSubject;
	}
	public String getOpinionDescription() {
		return opinionDescription;
	}
	public void setOpinionDescription(String opinionDescription) {
		this.opinionDescription = opinionDescription;
	}
	public String getOpinionType() {
		return opinionType;
	}
	public void setOpinionType(String opinionType) {
		this.opinionType = opinionType;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	
}

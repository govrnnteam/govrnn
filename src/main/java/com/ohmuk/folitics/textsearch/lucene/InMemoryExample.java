package com.ohmuk.folitics.textsearch.lucene;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class InMemoryExample {

	public static void main(String[] args) {
		Analyzer analyzer = new StandardAnalyzer();
		try {
			Directory index = new RAMDirectory();
			// Directory index = FSDirectory.open(new File("index-dir"));
			IndexWriterConfig config = new IndexWriterConfig(analyzer);
			IndexWriter writer = new IndexWriter(index, config);

			// Add some Document objects containing quotes
			String link1="www.ndtv.com";
			String link2="www.bbc.com";
			String link3="www.pti.com";
			createDocument(writer,link1,
					"Theodore Roosevelt",
					"It behooves every man to remember that the work of the "
							+ "critic, is of altogether secondary importance, and that, "
							+ "in the end, progress is accomplished by the man who does "
							+ "things.");
			createDocument(writer,link2,
					"Friedrich Hayek",
					"The case for individual freedom rests largely on the "
							+ "recognition of the inevitable and universal ignorance "
							+ "of all of us concerning a great many of the factors on "
							+ "which the achievements of our ends and welfare depend.");
			createDocument(writer,link3,
					"Ayn Rand",
					"There is nothing to take a man's freedom away from "
							+ "him, save other men. To be free, a man must be free "
							+ "of his brothers.");
			createDocument(writer,link3,
					"Mohandas Gandhi",
					"Freedom is not worth having if it does not connote "
							+ "freedom to err.");

			// Optimize and close the writer to finish building the index
			writer.close();

			String querystr = "Friedrich";
			//Query q = new QueryParser("title", analyzer).parse(querystr);
			Query q = new QueryParser("title", analyzer).parse(querystr);

			int hitsPerPage = 10;
			IndexReader reader = DirectoryReader.open(index);
			IndexSearcher searcher1 = new IndexSearcher(reader);
			TopScoreDocCollector collector = TopScoreDocCollector
					.create(hitsPerPage);
			searcher1.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			System.out.println("Query string: " + querystr);
			System.out.println("Found " + hits.length + " hits.");
			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document d = searcher1.doc(docId);
				System.out.println((i + 1) + ". " + d.get("course_code") + "\t"
						+ d.get("title"));
			}// Finally , close reader

		} catch (IOException ioe) {
			// In this example we aren't really doing an I/O, so this
			// exception should never actually be thrown.
			ioe.printStackTrace();
		} catch (org.apache.lucene.queryparser.classic.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createDocument(IndexWriter w, String link, String title, String courseCode)
			throws IOException {
		Document doc = new Document();
		doc.add(new TextField("link", link, Field.Store.YES));
		doc.add(new TextField("title", title, Field.Store.YES));
		// Here, we use a string field for course_code to avoid tokenizing.
		doc.add(new StringField("course_code", courseCode, Field.Store.YES));
		w.addDocument(doc);
	}

}
/**
 * 
 */
package com.ohmuk.folitics.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kalpana
 *
 */
public class UserUINotificationModel {
    private Long userId;
    private List<NotificationModel> notifications = new ArrayList<NotificationModel>();

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the notifications
     */
    public List<NotificationModel> getNotifications() {
        return notifications;
    }

    /**
     * @param notifications
     *            the notifications to set
     */
    public void setNotifications(List<NotificationModel> notifications) {
        this.notifications = notifications;
    }

}

package com.ohmuk.folitics.mongodb.service;

import com.ohmuk.folitics.mongodb.entity.NotificationMappingMongo;

public interface INotificationMappingMongoservice {

    public NotificationMappingMongo save(NotificationMappingMongo notificationMapping);

    public boolean deleteByUserId(Long userId);
}

package com.ohmuk.folitics.dao.implementations;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ohmuk.folitics.dao.interfaces.NewsFeedDao;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;

@Repository("newsFeedDao")
@Transactional
public class NewsFeedDaoImpl implements NewsFeedDao {

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public void save(SentimentNews sentimentNews) {
		getSession().createSQLQuery("set names 'UTF8'").executeUpdate();
		getSession().createSQLQuery("set character set 'UTF8'").executeUpdate();
		getSession().save(sentimentNews);
	}

	@Override
	public void update(SentimentNews sentimentNews) {
		getSession().update(sentimentNews);
	}
	
	@Override
	public void delete(SentimentNews sentimentNews) {
		getSession().delete(sentimentNews);
	}
}

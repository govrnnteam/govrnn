package com.ohmuk.folitics.dao.interfaces;

import com.ohmuk.folitics.hibernate.entity.SentimentNews;

public interface NewsFeedDao {
	public void save(SentimentNews sentimentNews);

	void update(SentimentNews sentimentNews);

	void delete(SentimentNews sentimentNews);
}

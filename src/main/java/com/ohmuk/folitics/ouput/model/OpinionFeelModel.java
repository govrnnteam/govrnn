package com.ohmuk.folitics.ouput.model;

public class OpinionFeelModel {

	private String positiveFeel;
	private String negativeFeel;
	private long positiveFeelCount;
	private long negativeFeelCount;
	
	public String getPositiveFeel() {
		return positiveFeel;
	}
	public void setPositiveFeel(String positiveFeel) {
		this.positiveFeel = positiveFeel;
	}
	public String getNegativeFeel() {
		return negativeFeel;
	}
	public void setNegativeFeel(String negativeFeel) {
		this.negativeFeel = negativeFeel;
	}
	public long getPositiveFeelCount() {
		return positiveFeelCount;
	}
	public void setPositiveFeelCount(long positiveFeelCount) {
		this.positiveFeelCount = positiveFeelCount;
	}
	public long getNegativeFeelCount() {
		return negativeFeelCount;
	}
	public void setNegativeFeelCount(long negativeFeelCount) {
		this.negativeFeelCount = negativeFeelCount;
	}
	
	
}

package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;

public class GeneralNotificationOutputModel extends BaseOutputModel<Object> implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private String componetType;

    private String action;

    private UserOutputModel user;

    private String notificationId;

    private String status;
    
    private String url;
    
    private String imageUrl;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * @return the componetType
     */
    public String getComponetType() {
        return componetType;
    }

    /**
     * @param componetType
     *            the componetType to set
     */
    public void setComponetType(String componetType) {
        this.componetType = componetType;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action
     *            the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    public UserOutputModel getUser() {
        return user;
    }

    public void setUser(UserOutputModel user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GeneralNotificationOutputModel() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    

    /*
     * public GeneralNotificationOutputModel(Long id, String title, String type,
     * UIMetrics uiMetrics, List<Opinion> otherAttributes, String fromattedAge)
     * { super(id, title, type, uiMetrics, otherAttributes, fromattedAge); //
     * TODO Auto-generated constructor stub }
     * 
     * 
     * public static final GeneralNotificationOutputModel getModel(Sentiment
     * entity) { GeneralNotificationOutputModel model = new
     * GeneralNotificationOutputModel(entity.getId(), null,
     * entity.getType(),null, null, null); model.setImage(entity.getImage());
     * model.setSentimentType(entity.getType());
     * model.setSubject(entity.getSubject()); return model; }
     */

}

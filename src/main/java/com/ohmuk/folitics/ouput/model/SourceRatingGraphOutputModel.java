/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;

/**
 * @author Deewan
 *
 */
public class SourceRatingGraphOutputModel extends BaseOutputModel<SentimentNewsRating> implements
Serializable  {
	
	private static final long serialVersionUID = 1L;	
	private RatingId ratingId;
	private long parentId;
	private Timestamp createTime;
	


	public RatingId getRatingId() {
		return ratingId;
	}


	public void setRatingId(RatingId ratingId) {
		this.ratingId = ratingId;
	}


	public long getParentId() {
		return parentId;
	}


	public void setParentId(long parentId) {
		this.parentId = parentId;
	}


	public Timestamp getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public SourceRatingGraphOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<SentimentNewsRating> otherAttributes,
			String fromattedAge) {
		super(id,title,type,uiMetrics,otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	public static final SourceRatingGraphOutputModel getModel(SentimentNewsRating entity) {
		SourceRatingGraphOutputModel model = new SourceRatingGraphOutputModel(null,null, null, null, null, null);
		model.setRatingId(entity.getRatingId());
		model.setCreateTime(entity.getCreateTime());
		return model;
}
}
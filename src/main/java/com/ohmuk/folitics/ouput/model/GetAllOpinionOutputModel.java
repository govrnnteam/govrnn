/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.StringUtils;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;

/**
 * @author Kalpana
 *
 */
public class GetAllOpinionOutputModel extends BaseOutputModel<String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String subject;
	private String text;
	private Attachment attachment;
	private Long created_By;
	private PollOutputModel agreeDisagreePoll;
	private UserOutputModel user;
	//location ?? need to be added
	private byte[] image;
	private String imageUrl;
		
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @return the agreeDisagreePoll
	 */
	public PollOutputModel getAgreeDisagreePoll() {
		return agreeDisagreePoll;
	}

	/**
	 * @param agreeDisagreePoll the agreeDisagreePoll to set
	 */
	public void setAgreeDisagreePoll(PollOutputModel agreeDisagreePoll) {
		this.agreeDisagreePoll = agreeDisagreePoll;
	}

	public GetAllOpinionOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the attachment
	 */
	public Attachment getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the created_By
	 */
	public Long getCreated_By() {
		return created_By;
	}

	/**
	 * @param created_By the created_By to set
	 */
	public void setCreated_By(Long created_By) {
		this.created_By = created_By;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public static final GetAllOpinionOutputModel getModel(Opinion entity) {
		GetAllOpinionOutputModel model = new GetAllOpinionOutputModel(entity.getId(),
				null, entity.getType(), null, null, entity.getFormattedAge());
		model.setSubject(entity.getSubject());
		model.setText(entity.getText());
		if(entity.getAttachment() != null)
			model.setAttachment(entity.getAttachment());
		model.setCreated_By(entity.getCreated_By());
		if(entity.getAgreeDisagrePoll() != null)
			model.setAgreeDisagreePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		model.setUser(UserOutputModel.getModel(entity.getUser()));
		model.setImageUrl(entity.getImageUrl());
		if(StringUtils.isEmpty(model.getImageUrl())){
			model.setImage(entity.getSentiment().getImage());
		}
		return model;
	}

	public UserOutputModel getUser() {
		return user;
	}

	public void setUser(UserOutputModel user) {
		this.user = user;
	}
}

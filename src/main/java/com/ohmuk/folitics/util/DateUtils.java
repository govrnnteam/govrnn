package com.ohmuk.folitics.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {

	protected static final Logger LOGGER = LoggerFactory
			.getLogger(DateUtils.class);

	private static Long dayToMiliseconds(int days) {
		Long result = Long.valueOf(days * 24 * 60 * 60 * 1000);
		return result;
	}

	public static Timestamp addDays(int days, Timestamp time) {
		try {
			if (days < 0) {
				throw new Exception("Day in wrong format.");
			}
			Long miliseconds = dayToMiliseconds(days);
			return new Timestamp(time.getTime() + miliseconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}

	public static final Timestamp getSqlTimeStamp() {
		Calendar cal = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(cal.getTime().getTime());
		Timestamp ts = new Timestamp(date.getTime());
		return ts;
	}

	public static final Timestamp getSqlTimeStamp(long minusTime) {
		Calendar cal = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(cal.getTime().getTime()
				- minusTime);
		Timestamp ts = new Timestamp(date.getTime());
		return ts;
	}

	public static int calculateAge(Date birthDate, Date currentDate) {
		if ((birthDate != null) && (currentDate != null)) {
			LocalDate birthDateLocal = birthDate.toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate currentDateLocal = currentDate.toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			return Period.between(birthDateLocal, currentDateLocal).getYears();
		} else {
			return 0;
		}
	}

	public static String calculateAgeWithString(Date birthDate) {
		int yrs = calculateAge(birthDate, new Date());
		if (yrs == 0 || yrs == 1) {
			return yrs + " Year";
		} else
			return yrs + " Years";
	}

	public static String getTimeLine(Date createDate) {

		if (createDate == null) {
			return null;
		} else
			return new SimpleDateFormat("MMM yyyy").format(createDate);

	}

	public static final String getDateOrTimeAgo(Timestamp timestamp) {
		if (timestamp == null)
			return "";
		Date date1 = new Date(timestamp.getTime());
		Date date2 = new Date();
		final int MILISECONDS_TO_MINS = 1000 * 60;
		long miliSeconds = date2.getTime() - date1.getTime();
		final int MILLI_TO_HOUR = MILISECONDS_TO_MINS * 60;
		long mins = miliSeconds / MILISECONDS_TO_MINS;
		if (mins == 0)
			return String.valueOf(1) + " Min ago";
		if (mins < 60) {
			return String.valueOf(mins) + " Mins ago";
		}
		long hours = miliSeconds / MILLI_TO_HOUR;
		if (hours == 1) {
			return String.valueOf(hours) + " Hour ago";
		}
		if (hours < 24) {
			return String.valueOf(hours) + " Hours ago";
		} else {
			int days = (int) ((date2.getTime() - date1.getTime()) / (MILLI_TO_HOUR * 24));
			if (days == 1) {
				return String.valueOf(days) + " Day ago";
			}
			if (days < 7) {
				return String.valueOf(days) + " Days ago";
			}
			SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
			return df.format(date1.getTime());
		}

	}
}

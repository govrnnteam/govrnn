package com.ohmuk.folitics.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

/**
 * 
 * @author Jahid Ali
 */

public class ThumbnailUtil {

    private static final int STANDARD_USER_IMAGE_WIDTH = 300;
    
    private static final int STANDARD_USER_IMAGE_HEIGHT = 300;

    private static final int STANDARD_THUMBNAIL_WIDTH = 100;
    
    private static final int STANDARD_THUMBNAIL_HEIGHT = 100;
    
    private static final int SENTIMENT_THUMBNAIL_WIDTH = 200;
    
    private static final int SENTIMENT_THUMBNAIL_HEIGHT = 200;
    
    private static final int STANDARD_ORIGINAL_WIDTH = 300;
    
    private static final int STANDARD_ORIGINAL_HEIGHT = 300;
    
    private static final int STANDARD_SNAPSHOT_WIDTH = 25;
    
    private static final int STANDARD_SNAPSHOT_HEIGHT = 25;
    
    public static byte[] getUserImage(byte[] imageInByte, String imageType) throws IOException {
        return getImageThumbnail(imageInByte, imageType, STANDARD_USER_IMAGE_WIDTH, STANDARD_USER_IMAGE_HEIGHT);
    }

    public static byte[] getImageThumbnail(byte[] imageInByte, String imageType) throws IOException {
        return getImageThumbnail(imageInByte, imageType, STANDARD_THUMBNAIL_WIDTH, STANDARD_THUMBNAIL_HEIGHT);
    }
    
    public static byte[] getSentimentImageThumbnail(byte[] imageInByte, String imageType) throws IOException {
        return getImageThumbnail(imageInByte, imageType, SENTIMENT_THUMBNAIL_WIDTH, SENTIMENT_THUMBNAIL_HEIGHT);
    }
        
    public static byte[] getImageOriginal(byte[] imageInByte, String imageType) throws IOException {
        return getImageThumbnail(imageInByte, imageType, STANDARD_ORIGINAL_WIDTH, STANDARD_ORIGINAL_HEIGHT);
    }
    
    public static byte[] getImageSnapshot(byte[] imageInByte, String imageType) throws IOException {
        return getImageThumbnail(imageInByte, imageType, STANDARD_SNAPSHOT_WIDTH, STANDARD_SNAPSHOT_HEIGHT);
    }

    private static BufferedImage getBufferedImage(byte[] imageInByte) throws IOException{
        InputStream in = new ByteArrayInputStream(imageInByte);
        BufferedImage image = ImageIO.read(in);
        return image;
    }

    public static byte[] getImageThumbnail(byte[] fullImageBytes, String imageType, int width, int height)
            throws IOException {
        BufferedImage fullImage= getBufferedImage(fullImageBytes);
        return getImageThumbnail(fullImage, imageType,width,height);
    }
    
    public static byte[] getImageThumbnail(BufferedImage fullImage, String imageType, int width, int height)
            throws IOException {
        
        // Quality indicate that the scaling implementation should do everything
        // create as nice of a result as possible , other options like speed
        // will return result as fast as possible
        // Automatic mode will calculate the resultant dimensions according
        // to image orientation .so resultant image may be size of 50*36.if you
        // want
        // fixed size like 50*50 then use FIT_EXACT
        // other modes like FIT_TO_WIDTH..etc also available.
        BufferedImage thumbImg = Scalr.resize(fullImage, Method.QUALITY, Mode.AUTOMATIC, width, height,
                Scalr.OP_ANTIALIAS);
        
        // convert bufferedImage to output stream
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(thumbImg, imageType, os);
        return os.toByteArray();
    }

}
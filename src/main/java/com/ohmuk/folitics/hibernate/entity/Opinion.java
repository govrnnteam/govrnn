package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * Entity implementation class for Entity: Opinion
 * 
 * @author Abhishek
 */
@Entity
@Table(name = "opinion")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
@PrimaryKeyJoinColumn(name = "id")
public class Opinion extends Component implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 200)
    @NotNull(message = "error.opinion.subject.notNull")
    @Size(min = 1, max = 200, message = "error.opinion.subject.size")
    private String subject;

    @Column(nullable = false, length = 2500)
    @NotNull(message = "error.opinion.text.notNull")
    @Size(min = 1, max = 2500, message = "error.opinion.text.size")
    private String text;

    @Column(length = 25, columnDefinition = "enum('Anti','Pro')")
    @NotNull(message = "error.opinion.type.notNull")
    private String type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "categoryId", referencedColumnName = "id")
    @NotNull(message = "error.opinion.category.notNull")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sentimentId", referencedColumnName = "id")
    @NotNull(message = "error.opinion.sentiment.notNull")
    private Sentiment sentiment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "useropinion", joinColumns = { @JoinColumn(name = "opinionId", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "userId", referencedColumnName = "id") })
    //@NotNull(message = "error.opinion.user.notNull")
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "linkId", referencedColumnName = "id")
    @Cascade(value = CascadeType.ALL)
    private Link link;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "attachmentId", referencedColumnName = "id")
    @Cascade(value = CascadeType.ALL)
    private Attachment attachment;

    @Column(nullable = false, length = 25, columnDefinition = "enum('Active','Deleted')")
    private String state;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "opinion")
    @JsonBackReference
//    @Cascade(value = CascadeType.ALL)
    private List<Response> responses;

    @Column(columnDefinition = "int default 0")
    private Integer statusgpi;

    @Column(length = 250)
    private String embeddedLink;

    @Column
    private Long componentId;

   /* @Column
    private Long views;*/

    @Column(nullable = false, columnDefinition = "enum ('Sentiment','Reference','SentimentNews')")
    private String onComponentType;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pollId", referencedColumnName = "id")
    @Cascade(value = CascadeType.ALL)
    private Poll agreeDisagrePoll;

    @Column(nullable = true)
    private String imageUrl;
    
    @Transient
    private List<Long> userIds;

    
    
    public Opinion() {
        setComponentType(ComponentType.OPINION.getValue());
        setCreateTime(DateUtils.getSqlTimeStamp());
        setEditTime(DateUtils.getSqlTimeStamp());
        setState(ComponentState.ACTIVE.getValue());
        setStatusgpi(0);
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    //
    // public Component getComponent() {
    // return component;
    // }
    //
    // public void setComponent(Component component) {
    // this.component = component;
    // }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

    /**
     * @return the statusgpi
     */
    public Integer getStatusgpi() {
        return statusgpi;
    }

    /**
     * @param statusgpi the statusgpi to set
     */
    public void setStatusgpi(Integer statusgpi) {
        this.statusgpi = statusgpi;
    }

    /**
     * @return the embeddedLink
     */
    public String getEmbeddedLink() {
        return embeddedLink;
    }

    /**
     * @param embeddedLink
     *            the embeddedLink to set
     */
    public void setEmbeddedLink(String embeddedLink) {
        this.embeddedLink = embeddedLink;
    }

    /**
     * @return the onComponentType
     */
    public String getOnComponentType() {
        return onComponentType;
    }

    /**
     * @param onComponentType
     *            the onComponentType to set
     */
    public void setOnComponentType(String onComponentType) {
        this.onComponentType = onComponentType;
    }

    /**
     * @return the componentId
     */
    public Long getComponentId() {
        return componentId;
    }

    /**
     * @param componentId
     *            the componentId to set
     */
    public void setComponentId(Long componentId) {
        this.componentId = componentId;
    }

    /**
     * @return the agreeDisagrePoll
     */
    public Poll getAgreeDisagrePoll() {
        return agreeDisagrePoll;
    }

    /**
     * @param agreeDisagrePoll
     *            the agreeDisagrePoll to set
     */
    public void setAgreeDisagrePoll(Poll agreeDisagrePoll) {
        this.agreeDisagrePoll = agreeDisagrePoll;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}

package com.ohmuk.folitics.hibernate.entity.newsfeed;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ohmuk.folitics.util.DateUtils;

/**
 * Entity implementation class for Entity: NewsSource
 * 
 * @author Jahid Ali
 */
@Entity
@Table(name = "feedsource")
@NamedQuery(name = "FeedSource.findAll", query = "SELECT r FROM FeedSource r")
public class FeedSource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false, length = 200)
    private String name;

    @Column(nullable = true, length = 200)
    private String company;

    @Column(nullable = true, length = 1000)
    private String description;

    @Column(nullable = true, length = 1000)
    private String popularRegion;

    @Column(nullable = true, length = 1000)
    private String bestSegment;
    
    @Column(nullable = true, length = 1000)
    private String frequency;//Daily, Weekly, Monthly
    
    @Column(nullable = true, length = 1000)
    private int day;//day of week, Sunday=1, 
    
    @Column(nullable = false, length = 1000)
    private String feedURL;

    @Column(nullable = false)
    private Boolean disabled;

    @Column(nullable = false)
    private Integer records;

    @Column(nullable = false)
    private Integer govrnnRanking;
   
    @Column(nullable = false)
    private String language;

    @Column(nullable = false)
    private String originCountry;

    @Column(nullable = false)
    private String type;//Aggregator, Source

    @Column(nullable = false)
    private boolean isRSSExposed;
    
    @Column(nullable = false)
    private String scheduleCron;
   
    @Column(nullable = false)
    private Timestamp created;
    
    @OneToMany(mappedBy = "feedSource", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonManagedReference(value = "feedSource-channel")
    private Set<FeedChannel> feedChannels;

    public Set<FeedChannel> getFeedChannels() {
        return feedChannels;
    }

    public void setFeedChannels(Set<FeedChannel> feedChannels) {
        this.feedChannels = feedChannels;
    }

    public FeedSource() {
        setCreated(DateUtils.getSqlTimeStamp());
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeedURL() {
        return feedURL;
    }

    public void setFeedURL(String feedURL) {
        this.feedURL = feedURL;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public String getScheduleCron() {
        return scheduleCron;
    }

    public void setScheduleCron(String scheduleCron) {
        this.scheduleCron = scheduleCron;
    }
    
    public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPopularRegion() {
		return popularRegion;
	}

	public void setPopularRegion(String popularRegion) {
		this.popularRegion = popularRegion;
	}

	public String getBestSegment() {
		return bestSegment;
	}

	public void setBestSegment(String bestSegment) {
		this.bestSegment = bestSegment;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Integer getGovrnnRanking() {
		return govrnnRanking;
	}

	public void setGovrnnRanking(Integer govrnnRanking) {
		this.govrnnRanking = govrnnRanking;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isRSSExposed() {
		return isRSSExposed;
	}

	public void setRSSExposed(boolean isRSSExposed) {
		this.isRSSExposed = isRSSExposed;
	}

	public static final FeedSource getFeedSource(String name, String url) {
        FeedSource feedSource = new FeedSource();
        feedSource.setDescription(name);
        feedSource.setDisabled(Boolean.FALSE);
        feedSource.setFeedURL(url);
        feedSource.setName(name);
        feedSource.setRecords(50);// Default
        feedSource.setScheduleCron("0 0 0/2 1/1 * ? *");// Default
        return feedSource;
    }
}

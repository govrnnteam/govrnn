package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FoliticsUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * Entity implementation class for Entity: User
 * 
 * @author Mayank Sharma
 *
 */
@Entity
@Table(name = "user")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class User implements Serializable, Comparable<User> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(unique = true, nullable = false, length = 255)
	@NotNull(message = "error.user.username.notNull")
	@Size(min = 1, max = 255, message = "error.user.username.size")
	private String username;

	@Column(nullable = false, length = 255)
	@NotNull(message = "error.user.password.notNull")
	@Size(min = 1, max = 255, message = "error.user.password.size")
	private String password;

	@Column(nullable = false, length = 512)
	@NotNull(message = "error.user.emailId.notNull")
	@Size(min = 1, max = 512, message = "error.user.emailId.size")
	private String emailId;

	@Column(nullable = false, length = 512)
	@NotNull(message = "error.user.name.notNull")
	@Size(min = 1, max = 512, message = "error.user.name.size")
	private String name;

	@Column(nullable = false, length = 255, columnDefinition = "enum('Male','Female','Other')")
	@NotNull(message = "error.user.gender.notNull")
	@Size(min = 1, max = 255, message = "error.user.gender.size")
	private String gender;
	
	@Column(nullable = false, length = 128, columnDefinition = "enum('VerifiedByEmail','Pending_Verification','VerifiedByBoth','Suspended','Active','Disabled','Deleted')")
	private String status;

	@Column(nullable = true)
	private Timestamp dob;

	@Column(nullable = true, length = 255)
	private String maritalStatus;

	@Column(nullable = true, length = 255)
	private String state;

	@Column(nullable = true, length = 256)
	private String religion;

	@Column(nullable = false)
	private Timestamp createTime;

	@Column(nullable = true,length = 2000)
	private String summary;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<UserAssociation> userAssociation;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<UserEducation> userEducation;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<UserEmailNotificationSettings> userEmailNotificationSettings;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private List<ContactDetails> contactDetails;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<UserUINotification> userUINotification;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private List<UserImage> userImages;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<Achievement> achievements;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<Leader> leaders;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	private Set<PoliticalView> politicalViews;

	@Column(nullable = true)
	private String googlePlusEmail;

	@Column(nullable = true)
	private String faceBookEmail;

	@Column(nullable = true)
	private String twitterEmail;

	@Column(nullable = true)
	private String website;

	/*
	 * @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	 * 
	 * @Cascade(CascadeType.ALL) private Set<UserPrivacyData>
	 * userPrivacySettings;
	 */

	@Column(nullable = true)
	private Double points;

	@Column(nullable = true)
	private String badge;

	@Column(nullable = true)
	private Long inclinationAggregation;

	@Column(nullable = true)
	private String caste;

	@Column(nullable = true, length = 255)
	private String qualification;

	@Column(nullable = true)
	private Long mobileNumber;

	@Column(nullable = true, length = 1024)
	private String hobbies;

	@Column(nullable = true, length = 255)
	private String city;

	@Column(nullable = true, length = 255)
	private String country;

	@Column(nullable = true, length = 255)
	private String currentLocation;

	@Column(nullable = true, length = 255)
	private String nationality;

	@Column(nullable = true, length = 255)
	private String occupation;

	@Column(nullable = true, length = 255)
	private String motherTongue;

	@Column(nullable = true, length = 255)
	private String address;

	@ManyToOne(fetch = FetchType.EAGER)
	private UserRole role;

	@Column(nullable = true, length = 255)
	private String emePersonName;

	@Column(nullable = true, length = 255)
	private String emeContactNo;
	
	@Column(nullable = true, length = 255)
	private String nativeCity;

	public User() {
		super();

		setCreateTime(DateUtils.getSqlTimeStamp());
	}

	public User(Long id, String username, String password, String emailId,
			String name) {
		super();
		setCreateTime(DateUtils.getSqlTimeStamp());

		this.id = id;
		this.username = username;
		this.password = password;
		this.emailId = emailId;
		this.name = name;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Set<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(Set<Achievement> achievements) {
		this.achievements = achievements;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the userAssociation
	 */
	public Set<UserAssociation> getUserAssociation() {
		return userAssociation;
	}

	/**
	 * @param userAssociation
	 *            the userAssociation to set
	 */
	public void setUserAssociation(Set<UserAssociation> userAssociation) {
		this.userAssociation = userAssociation;
	}

	/**
	 * @return the userEducation
	 */
	public Set<UserEducation> getUserEducation() {
		return userEducation;
	}

	/**
	 * @param userEducation
	 *            the userEducation to set
	 */
	public void setUserEducation(Set<UserEducation> userEducation) {
		this.userEducation = userEducation;
	}

	/**
	 * @return the userEmailNotificationSettings
	 */
	public Set<UserEmailNotificationSettings> getUserEmailNotificationSettings() {
		return userEmailNotificationSettings;
	}

	/**
	 * @param userEmailNotificationSettings
	 *            the userEmailNotificationSettings to set
	 */
	public void setUserEmailNotificationSettings(
			Set<UserEmailNotificationSettings> userEmailNotificationSettings) {
		this.userEmailNotificationSettings = userEmailNotificationSettings;
	}

	/**
	 * @return the userUINotification
	 */
	public Set<UserUINotification> getUserUINotification() {
		return userUINotification;
	}

	/**
	 * @param userUINotification
	 *            the userUINotification to set
	 */
	public void setUserUINotification(Set<UserUINotification> userUINotification) {
		this.userUINotification = userUINotification;
	}

	/**
	 * @return the userImages
	 */
	public List<UserImage> getUserImages() {
		return userImages;
	}

	/**
	 * @param userImage
	 *            the userImage to set
	 */
	public void setUserImages(List<UserImage> userImages) {
		this.userImages = userImages;
	}

	/*    *//**
	 * @return the userPrivacySettings
	 */
	/*
	 * public Set<UserPrivacyData> getUserPrivacySettings() { return
	 * userPrivacySettings; }
	 *//**
	 * @param userPrivacySettings
	 *            the userPrivacySettings to set
	 */
	/*
	 * public void setUserPrivacySettings(Set<UserPrivacyData>
	 * userPrivacySettings) { this.userPrivacySettings = userPrivacySettings; }
	 */

	/**
	 * @return the points
	 */
	public Double getPoints() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void setPoints(Double points) {
		this.points = points;
	}

	/**
	 * @return the badge
	 */
	public String getBadge() {
		return badge;
	}

	/**
	 * @param badge
	 *            the badge to set
	 */
	public void setBadge(String badge) {
		this.badge = badge;
	}

	/**
	 * @return the inclinationAggregation
	 */
	public Long getInclinationAggregation() {
		return inclinationAggregation;
	}

	/**
	 * @param inclinationAggregation
	 *            the inclinationAggregation to set
	 */
	public void setInclinationAggregation(Long inclinationAggregation) {
		this.inclinationAggregation = inclinationAggregation;
	}

	/**
	 * @return the role
	 */
	public UserRole getRole() {
		return role;
	}

	/**
	 * @return the caste
	 */
	public String getCaste() {
		return caste;
	}

	/**
	 * @param caste
	 *            the caste to set
	 */
	public void setCaste(String caste) {
		this.caste = caste;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(UserRole role) {
		this.role = role;
	}

	/**
	 * @return the dob
	 */
	public Timestamp getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	/**
	 * @return the maritalStatus
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * @param maritalStatus
	 *            the maritalStatus to set
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the religion
	 */
	public String getReligion() {
		return religion;
	}

	/**
	 * @param religion
	 *            the religion to set
	 */
	public void setReligion(String religion) {
		this.religion = religion;
	}

	/**
	 * @return the leaders
	 */
	public Set<Leader> getLeaders() {
		return leaders;
	}

	/**
	 * @param leaders
	 *            the leaders to set
	 */
	public void setLeaders(Set<Leader> leaders) {
		this.leaders = leaders;
	}

	/**
	 * @return the politicalViews
	 */
	public Set<PoliticalView> getPoliticalViews() {
		return politicalViews;
	}

	/**
	 * @param politicalViews
	 *            the politicalViews to set
	 */
	public void setPoliticalViews(Set<PoliticalView> politicalViews) {
		this.politicalViews = politicalViews;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		if(StringUtils.isEmpty(this.qualification))
			return "-";
		return qualification;
	}

	/**
	 * @param qualification
	 *            the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the mobileNumber
	 */
	public Long getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the hobbies
	 */
	public String getHobbies() {
		return hobbies;
	}

	/**
	 * @param hobbies
	 *            the hobbies to set
	 */
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the currentLocation
	 */
	public String getCurrentLocation() {
		return currentLocation;
	}

	/**
	 * @param currentLocation
	 *            the currentLocation to set
	 */
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation
	 *            the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the motherTongue
	 */
	public String getMotherTongue() {
		return motherTongue;
	}

	/**
	 * @param motherTongue
	 *            the motherTongue to set
	 */
	public void setMotherTongue(String motherTongue) {
		this.motherTongue = motherTongue;
	}

	/**
	 * @return the contactDetails
	 */
	public List<ContactDetails> getContactDetails() {
		return contactDetails;
	}

	/**
	 * @param contactDetails
	 *            the contactDetails to set
	 */
	public void setContactDetails(List<ContactDetails> contactDetails) {
		this.contactDetails = contactDetails;
	}

	/**
	 * @return the Summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param Summary
	 *            the Summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getEmePersonName() {
		return emePersonName;
	}

	public void setEmePersonName(String emePersonName) {
		this.emePersonName = emePersonName;
	}

	public String getEmeContactNo() {
		return emeContactNo;
	}

	public void setEmeContactNo(String emeContactNo) {
		this.emeContactNo = emeContactNo;
	}

	@Override
	public int compareTo(User o) {
		if (getCreateTime() == null || o.getCreateTime() == null)
			return 0;
		return getCreateTime().compareTo(o.getCreateTime());
	}

	/**
	 * @return the googlePlusEmail
	 */
	public String getGooglePlusEmail() {
		return FoliticsUtils.sanitizeURL(googlePlusEmail);
	}

	/**
	 * @param googlePlusEmail the googlePlusEmail to set
	 */
	public void setGooglePlusEmail(String googlePlusEmail) {
		this.googlePlusEmail = googlePlusEmail;
	}

	/**
	 * @return the faceBookEmail
	 */
	public String getFaceBookEmail() {
		return FoliticsUtils.sanitizeURL(faceBookEmail);
	}

	/**
	 * @param faceBookEmail the faceBookEmail to set
	 */
	public void setFaceBookEmail(String faceBookEmail) {
		this.faceBookEmail = faceBookEmail;
	}

	/**
	 * @return the twitterEmail
	 */
	public String getTwitterEmail() {
		return FoliticsUtils.sanitizeURL(twitterEmail);
	}

	/**
	 * @param twitterEmail the twitterEmail to set
	 */
	public void setTwitterEmail(String twitterEmail) {
		this.twitterEmail = twitterEmail;
	}

	public String getWebsite() {
		return FoliticsUtils.sanitizeURL(website);
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getNativeCity() {
		return nativeCity;
	}

	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}

	
	
}

package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "newsfeed")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class NewsFeed implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "NewsFeedId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String language;

	@Column
	private String source;

	@Column
	private String SourceLink;

	@Column
	private String section;

	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany
	@Cascade(value = CascadeType.ALL)
	@JoinTable(name = "NEWSFEED_HEADINGS", joinColumns = { @JoinColumn(name = "NewsFeedId") }, inverseJoinColumns = { @JoinColumn(name = "NewsHeadingId") })
	private List<NewsHeading> newsHeading;

	@Column
	private String plainText;

	@Column
	private String category;

	@Column
	private String imageUrl;

	@Column
	private byte[] image;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceLink() {
		return SourceLink;
	}

	public void setSourceLink(String sourceLink) {
		SourceLink = sourceLink;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public List<NewsHeading> getNewsHeading() {
		return newsHeading;
	}

	public void setNewsHeading(List<NewsHeading> newsHeading) {
		this.newsHeading = newsHeading;
	}

	public String getPlainText() {
		return plainText;
	}

	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "NewsFeed [id=" + id + ", language=" + language + ", source=" + source + ", SourceLink=" + SourceLink
				+ ", section=" + section + ", newsHeading=" + newsHeading + ", plainText=" + plainText + ", category="
				+ category + ", imageUrl=" + imageUrl + ", image=" + Arrays.toString(image) + "]";
	}

}

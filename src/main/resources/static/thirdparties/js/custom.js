
$(document).ready(function(){
  (function($){
 	var cards = $(".card-drop"),
		toggler = cards.find(".toggle"),
		links = cards.find("ul>li>a"),
		li = links.parent('li'),
		count = links.length,
		width = links.outerWidth();

		//set z-Index of drop Items
		links.parent("li").each(function(i){
			$(this).css("z-index" , count - i); //invert the index values
		});

		//set top margins & widths of li elements
		function setClosed(){
			li.each(function(index){});
         	li.addClass('closed');
			toggler.removeClass("active");
		}
		setClosed();

	/* -------------------------------------------------------- */ 
	/*	Toggler Click handler
	/* -------------------------------------------------------- */ 
	toggler.on("mousedown" , function(){
		   var $this = $(this); //cache $(this)
		   console.info(this);
		if($this.is(".active")){
			setClosed();
		}else{
			//if the menu is un-active:
			$this.addClass("active");
			li.removeClass('closed');
			//set top margins
			li.each(function(index){
				 
			});
		}
	});

	links.on("click" , function(e){
		var $this = $(this),
			label = $this.data("label");
			icon = $this.children("i").attr("class");
			li.removeClass('active');
		if($this.parent("li").is("active")){
			$this.parent('li').removeClass("active");
		}else{
			$this.parent("li").addClass("active");
		}
		toggler.children("span").text(label);
		toggler.children("i").removeClass().addClass(icon);
		setClosed();
		e.preventDefault;
	});
	
	
	$(".full_bg").on("click", function (event) {
		setClosed();
	});
	
	$(".navbar-static-top").on("click", function (event) {
		setClosed();
	});
	
	$(".p_container").on("click", function (event) {
		setClosed();
	});

})(jQuery);


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$('.sentiment_container').carousel({
    interval: 0
});
$('#carousel-poll.carousel').carousel({
    interval: 0
});
$('.number_pop .carousel').carousel({
    interval: 0
});

$('#slide_vertical.carousel').carousel({
    interval: 0
});
$('#carousel-slider_box.carousel.slide').carousel({
    interval: 0
});
$('#carousel-related.carousel.slide').carousel({
    interval: 0
});
$('.submit_pro').click(function(){
	$('.submit_show').show(300);
	$('html, body').animate({scrollTop:140}, 'slow');
	
});
$('.coll').click(function(){
  $(this).next('.sub_lis').toggle();
  $(this).toggleClass('fa-times');
  $(this).toggleClass('fa-pencil');
});

var $radioButtons = $('.poll input[type="radio"]');
$radioButtons.click(function() {
    $radioButtons.each(function() {
        $(this).parent().addClass('select_poll', this.checked);
    });
});

$( ".basic_stp" ).click(function() {
   $("#sign_pop .nav.nav-tabs.register li").removeClass("active");
   $("#sign_pop .nav.nav-tabs.register li:nth-child(2)").addClass("active");
});

$( ".pro_stp" ).click(function() {
   $("#sign_pop .nav.nav-tabs.register li").removeClass("active");
   $("#sign_pop .nav.nav-tabs.register li:nth-child(3)").addClass("active");
});

$( ".box_sentiment a.link_sentiment" ).click(function() {
  $( ".show_onclick" ).show();
  $( ".narrow_show_onclick" ).show();
  $( ".how_to_play" ).hide();
  $( "#carousel-sentiment .item ul li .box_sentiment" ).removeClass('active');
  $(this).parent(".box_sentiment").addClass('active');
  $( ".collapse_sec" ).hide();
  $( ".top_sec_govt" ).hide();
  $('html, body').animate({scrollTop:460}, 'slow');
});

$( ".toggle_txt ul li.last a" ).click(function() {
		
	$( ".collapse_sec" ).show();
	$( ".show_onclick" ).hide();
});

$( "ul.left_nav li a.close_nav" ).click(function() {
  $("body").toggleClass( "highlight" );
});
$( ".round_text_file .filter_data li.alert" ).click(function() {
  $(".filter_data li.alert").children( ".detail_col" ).hide();
  $(this).children( ".detail_col" ).show();
});

$(".search_opin input").mouseover(function(){ 
 $(".col-pop-bx").show();
});

$(".answer").hide();
$("#has_q").click(function() {
    if($(this).is(":checked")) {
        $(".answer").show(300);
    } else {
        $(".answer").hide(200);
    }
});
$('.trend_cmnt').click(function(){
  $('.com_ur_byt').show(600);
});
$('.hid_par').click(function(){
  $('.com_ur_byt').hide(300);
});

$(".solve_problem .bg .inline .sty_rad input[type='radio']").click(function(){

    if($("#quick").is(":checked")) {
        $("#quickly").show();
    } else {
        $("#quickly").hide();
    }
});

if($("#which_govt").hasClass(".panel-collapse.collapse.in")){
	
	alert("ok");
	
}

$(".search_opin input").mouseout(function(){  $(".col-pop-bx").hide(); });

$( ".tab.narrow li a" ).click(function() { $(".search_fld").show(); });
$( ".view_sup a" ).click(function() { $(".respond_sec").show(); 
	$( ".view_sup a" ).addClass('disabled');
	$(this).removeClass('disabled');
	});

$( ".tab.narrow li a" ).click(function() {
	$( ".tab.narrow li a" ).removeClass('active');
	$(this).addClass('active');
	
});
	
$( ".number_value li.fool_nav" ).mouseover(function() {$( ".slider_box .carousel-indicators li p.fool" ).show(); });
$( ".number_value li.fool_nav" ).mouseout(function() { $( ".slider_box .carousel-indicators li p.fool" ).hide(); });

$( ".number_value li.bad_nav" ).mouseover(function() {$( ".slider_box .carousel-indicators li p.bad" ).show(); });
$( ".number_value li.bad_nav" ).mouseout(function() { $( ".slider_box .carousel-indicators li p.bad" ).hide(); });

$( ".number_value li.good_nav" ).mouseover(function() {$( ".slider_box .carousel-indicators li p.good" ).show(); });
$( ".number_value li.good_nav" ).mouseout(function() { $( ".slider_box .carousel-indicators li p.good" ).hide(); });

$( ".number_value li.excellent_nav" ).mouseover(function() {$( ".slider_box .carousel-indicators li p.excellent" ).show(); });
$( ".number_value li.excellent_nav" ).mouseout(function() { $( ".slider_box .carousel-indicators li p.excellent" ).hide(); });
$('a.s_mov[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
		    $('html, body').animate({
		      //scrollTop: (target.offset().top - 80 )
			  scrollTop: (target.offset().top - 10)
		    }, 1000);
		    return false;
		  }
		}
	});
}); 
$(document).ready(function(){
    // Defining the local dataset
    var sentiment = ['Beef', 'Beef ban', 'Education in India', 'Uri Attack', 'JNU', 'Delhi College'];
    
    // Constructing the suggestion engine
    var sentiment = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: sentiment
    });
    
    // Initializing the typeahead
    $('.typeahead').typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing result */
    },
    {
        name: 'sentiment',
        source: sentiment
    });
  

$('.datepicker').datepicker({
    startDate: '-3d'
});
$('#share_alt').popover('toggle');

$('.animation_text input').on('blur', function(){
   $(this).parent('.animation_text').removeClass('input-desc-hover');
   
}).on('focus', function(){
  $(this).parent('.animation_text').addClass('input-desc-hover');
});

$('.animation_text textarea').on('blur', function(){
   $(this).parent('.animation_text').removeClass('input-desc-hover');
   
}).on('focus', function(){
  $(this).parent('.animation_text').addClass('input-desc-hover');
});
$('.animation_text select').on('blur', function(){
   $(this).parent('.animation_text').removeClass('input-desc-hover');
   
}).on('focus', function(){
  $(this).parent('.animation_text').addClass('input-desc-hover');
});


$("#city").change(function(){
	var interest = document.getElementById("city");
	var interestValue = interest.options[interest.selectedIndex].value;
	if(interestValue!=' ') {
		$(this).parent().addClass('has_txt');
	}else {
		$(this).parent().removeClass('has_txt');
	}
});
$("#state").change(function(){
	var interest = document.getElementById("state");
	var interestValue = interest.options[interest.selectedIndex].value;
	if(interestValue!=' ') {
		$(this).parent().addClass('has_txt');
	}else {
		$(this).parent().removeClass('has_txt');
	}
});
$("#Religion").change(function(){
	var interest = document.getElementById("state");
	var interestValue = interest.options[interest.selectedIndex].value;
	if(interestValue!=' ') {
		$(this).parent().addClass('has_txt');
	}else {
		$(this).parent().removeClass('has_txt');
	}
});
$("#caste").change(function(){
	var interest = document.getElementById("state");
	var interestValue = interest.options[interest.selectedIndex].value;
	if(interestValue!=' ') {
		$(this).parent().addClass('has_txt');
	}else {
		$(this).parent().removeClass('has_txt');
	}
});
$("#edu").change(function(){
	var interest = document.getElementById("edu");
	var interestValue = interest.options[interest.selectedIndex].value;
	if(interestValue!=' ') {
		$(this).parent().addClass('has_txt');
	}else {
		$(this).parent().removeClass('has_txt');
	}
});

$('.animation_text.dob input').on('click', function(){
  $(this).parent('.animation_text').addClass('has_txt');
});
$('.next_pop').click(function(){
  $('.opinion_nxt_step').hide(300);
  $('.rebut_page').show(300);
});



var inputs = $('.animation_text input').not(':submit');

inputs.on('input', function() {
	$(inputs[inputs.index(this)]).parent().toggleClass('has_txt', this.value > '');
});
if(inputs)
 inputs[0].focus();


var summary = $('.animation_text textarea');
summary.on('textarea', function() {
	$(summary[summary.index(this)]).parent().toggleClass('has_txt', this.value > '');
});
summary[0].focus();
		

function getImageUrl(relativePath) {
	var fullPath =getContextPath()+relativePath;
	console.log(fullPath);
    return fullPath;
}

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}
console.log(getContextPath());





});




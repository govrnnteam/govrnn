'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
	.controller('opinionNotificationController', ['$http', '$scope', '$rootScope',	'SentimentService', 'HomeService', '$cookies',
		function ($http, $scope, $rootScope, SentimentService, HomeService, $cookies) {
		   var userId = $cookies.getObject('userID');
		   
			$http({
				method: 'GET',
				url: $rootScope.liveHost+"/notification/getOpinionNotifications",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				params: {userId: userId,componentType:'Opinion'}
			}).then(function successCallback(response) {
				debugger;
				$('#hola').hide();
				if(response.data.success === true){
					$scope.TopnotificationCount = response.data.messages;
				}else{
					$scope.hasError = true;
					$scope.user = {};
					form.$setPristine();
					console.log("succes:error login");
				}
			}, function errorCallback(response) {
				$scope.hasError = true;
				$scope.user = {};
				form.$setPristine();
				console.log("error login");
				console.log(response);
				$('#hola').hide();
			});
		}]);
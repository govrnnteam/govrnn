'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
  .controller('ViewAllOpinionCtrl', ['$http', '$scope', '$rootScope', 'SentimentService','$location','$sce',
  function ($http, $scope, $rootScope, SentimentService, $location, $sce) {
		console.log('refersh');	
	  $scope.tab = 1;
	  $scope.temporarySentimentsSets = [];
	  $scope.permanentSentimentsSets = [];
	  $scope.sentimentIdParam = ($location.search().id != null)? $location.search().id : 0;
	  $scope.sentimentIdType = ($location.search().type != null)? $location.search().type : "";
	  $scope.opinionSearchKey = ($location.search().search != null)? $location.search().search : null;
	  
	  $scope.opinionSearchKeyError = false;
	  
	  $scope.btnDisable =true;
	  //pagination 
	  $scope.totalPages = 0;
	  $scope.currentPage = 0;
	  $scope.pageSize = 10; // number Page Size 
	  $scope.opinionViewLatest =false;
	  var senIdSelectedForSearch ='';
	  var urlParams = $location.search();
	  console.log('urlParams.viewLatest='+urlParams.viewLatest);
	  if(urlParams.viewLatest != undefined)
	     $scope.opinionViewLatest = true;
		$scope.sentimentCauroselMinLength = 0;
		$scope.sentimentCauroselMaxLength = 8;

		$scope.tempSentimentCaurselFrom= $scope.sentimentCauroselMinLength ;
		$scope.permSentimentCaurselFrom = $scope.sentimentCauroselMinLength ;
		
		$scope.tempSentimentCaurselTo = $scope.sentimentCauroselMaxLength ;
		$scope.permSentimentCaurselTo= $scope.sentimentCauroselMaxLength ;


	   //searchCode Start
	  if(urlParams.search){
			$http({
				method : 'GET',
				url : $rootScope.liveHost
				+ "/opinion/searchOpinionbykeyword",
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				params : {
					search: urlParams.search,
					sentimentId:senIdSelectedForSearch
				}
			}).then(function successCallback(response) {
				console.log('getOpinion ::::' +senIdSelectedForSearch)
				if(response.data.success === true){
					console.log("success: Search Opinions");
				}else{
					console.log("Failure : Search Opinions");
				}
			}, function errorCallback(response) {
				console.log("Error: Search Opinions");
			});
		}
	  //SearchCode End
	  
	  // Loading the permanent Sentiments
	  $scope.sentimentFound = false;
	  
	  if($scope.sentimentIdParam != 0){
		  if($scope.sentimentIdType == "Temporary"){
			  setTemporarySentimentsSets ($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
			 // setPermanentSentimentsSets ($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
		  }else if ($scope.sentimentIdType == "Permanent"){
			  setPermanentSentimentsSets ($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
			 // setTemporarySentimentsSets ($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
		  }
	  }
	  else{
		 
		  $scope.tempSentimentCaurselFrom = $scope.sentimentCauroselMinLength;
		  $scope.tempSentimentCaurselTo = $scope.sentimentCauroselMaxLength;
		  
		  setTemporarySentimentsSets ($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
			
		 
	  }
	  
	  function setTemporarySentimentsSets (from,to){

			// Loading the temporary Sentiments
			SentimentService.findByTypeTemporary(from,to).then(function(response) {
				$scope.temporarySentiments = response;
				var data = response;
				var sentimentLength = data.length;
				if(sentimentLength > 0 && $scope.opinionSearchKey == null && $scope.opinionViewLatest==false){
					if($scope.sentimentIdParam ==0){
						$scope.displayOpinions('#current_', data[0].id);
						$scope.sentimentIdParam = data[0].id;
					}
				}	
				for (var i = 0; i < sentimentLength; i += 8) {
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
							// Heighlighting the passed sentiment tab
							if($scope.sentimentIdParam != 0){
								if(data[j].id == $scope.sentimentIdParam){
									$scope.sentimentFound = true;
									$scope.tab = 1;
								}
							}
						}
					}
					$scope.temporarySentimentsSets.push(newobj);
				}
				if(!$scope.sentimentFound && $scope.sentimentIdParam != 0){
					  $scope.tempSentimentCaurselFrom = $scope.tempSentimentCaurselTo;
					  $scope.tempSentimentCaurselTo += $scope.sentimentCauroselMaxLength;
					  setTemporarySentimentsSets($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
				}
				$("#carousel-viewall").carousel({interval: 3000, pause: "hover"});
			});
			
			
	  }
	  function setPermanentSentimentsSets (from,to){

		  SentimentService.findByTypePermanent(from,to).then(function(response) {
				$scope.permanentSentiments = response;
				var data = response;
				var sentimentLength = data.length;
				
			for (var i = 0; i < sentimentLength; i += 8) {
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
							// Heighlighting the passed sentiment tab
							if($scope.sentimentIdParam != 0){
								if(data[j].id == $scope.sentimentIdParam){
									$scope.sentimentFound = true;
									$scope.tab = 2;
								}
							}
						}
					}
					$scope.permanentSentimentsSets.push(newobj);
				}
				if(!$scope.sentimentFound && $scope.sentimentIdParam != 0 ){
					  $scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo;
					  $scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength;
					  setPermanentSentimentsSets($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
				}
				$("#carousel-viewall-perm").carousel({interval: 3000, pause: "hover"}); 
			});
	
	  }	  
	  // The below functions are used for tab and slide functionalities
		$scope.setTab = function(newTab){
				$scope.tab = newTab;
				if($scope.tab == 2){
					loadPermanentSentiemnts();
				}
			};

		$scope.isSet = function(tabNum){
			return $scope.tab === tabNum;
		};
		
		function loadPermanentSentiemnts(){
			
			if($scope.permanentSentimentsSets.length == 0){
				 $scope.permSentimentCaurselFrom = $scope.sentimentCauroselMinLength;
				  $scope.permSentimentCaurselTo = $scope.sentimentCauroselMaxLength;
				  
				  setPermanentSentimentsSets ($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
			
			}				 
		};
		$scope.prev = function(slide) {
			
			$('.sentiment_container .carousel-control')[0].style.background='red';
			$('.sentiment_container .carousel-control')[1].style.background='#f6f5f5';
			    
			
			if(slide == '#carousel-viewall-perm'  ){
				if($scope.permSentimentCaurselTo != $scope.sentimentCauroselMaxLength){
					$scope.permSentimentCaurselFrom -= $scope.sentimentCauroselMaxLength ;						
					$scope.permSentimentCaurselTo -= $scope.sentimentCauroselMaxLength ;

				}					
			}
			else if(slide == '#carousel-viewall'){
				if($scope.tempSentimentCaurselTo != $scope.sentimentCauroselMaxLength){
					$scope.tempSentimentCaurselFrom -= $scope.sentimentCauroselMaxLength ;						
					$scope.tempSentimentCaurselTo -= $scope.sentimentCauroselMaxLength ;

				}								
			}
			$(slide).carousel("prev");
		};
		$scope.next = function(slide) {
			debugger;
			$('.sentiment_container .carousel-control')[1].style.background='red';
			$('.sentiment_container .carousel-control')[0].style.background='#f6f5f5';
			
			if(slide == '#carousel-viewall-perm'){				 
				
				if($scope.permSentimentCaurselTo < $scope.totalPermSentiemnts){

					$scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo ;						
					$scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

					if($scope.permSentimentCaurselTo >  ($scope.permanentSentimentsSets.length * 8 ) ){

						SentimentService.findByTypePermanent($scope.permSentimentCaurselFrom ,$scope.permSentimentCaurselTo).then(function(response) {
							//console.log('called per');

							$scope.permanentSentiments = response;
							var data = response;
							var sentimentLength = data.length;

							for (var i = 0; i < sentimentLength; i += 8) {
								//console.log(i);
								var newobj = {};
								for(var j=i; j< (i+8); j++){
									if(data[j]){
										newobj[j]= data[j];
									}
								}
								$scope.permanentSentimentsSets.push(newobj);
							}
							$(slide).carousel("next");
						});

					}
					else{
						$(slide).carousel("next");
					}
				}
			}else if(slide == '#carousel-viewall'){

				if($scope.tempSentimentCaurselTo < $scope.totalTempSentiemnts){

					$scope.tempSentimentCaurselFrom = $scope.tempSentimentCaurselTo ;						
					$scope.tempSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

					if($scope.tempSentimentCaurselTo >  ($scope.temporarySentimentsSets.length * 8 ) ){

						SentimentService.findByTypeTemporary($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo).then(function(response) {
							$scope.temporarySentiments = response;
							var data = response;
							var sentimentLength = data.length;

							for (var i = 0; i < sentimentLength; i += 8) {
								var newobj = {};
								for(var j=i; j< (i+8); j++){
									if(data[j]){
										newobj[j]= data[j];
									}
								}
								$scope.temporarySentimentsSets.push(newobj);
							}
							$(slide).carousel("next");
						});

					}else{
						$(slide).carousel("next");
					}
				}					
			
			}
		
			
		
			$(slide).carousel("next");
		};
		var pauseSlides = function(){
			$("#carousel-viewall").carousel("pause");
			$("#carousel-viewall-perm").carousel("pause");
		};
		
		var getOpinion = function(sentimentId, isSearch, viewLatest){
			var apiUrl;
			if(isSearch == true){
				console.log('Searching for opinions');
				apiUrl = '/opinion/searchOpinionbykeyword?search='+sentimentId+'&sentimentId='+senIdSelectedForSearch+'';
			}			
			if(urlParams.viewLatest && viewLatest){
				console.log('Loading latest opinions');
				apiUrl = '/opinion/getLatestOpinions?count='+urlParams.viewLatest
			}
			else{
				console.log('Loading opinions for sentiment');
				apiUrl = '/opinion/getOpinionsModel?id='+sentimentId
			}
			$http.get(apiUrl).then(function(response) {
				if (response.data.messages != null) {
					var tmpOpins = response.data.messages;
					angular.forEach(tmpOpins,function(opinion, index){
						tmpOpins[index].opinionText = $sce.trustAsHtml(opinion.text);
						if(opinion.agreeDisagrePoll != undefined){
							angular.forEach(opinion.agreeDisagrePoll.pollOptionOutputModels, function(opt){
								if(opt.pollOption == "Yes"){
									tmpOpins[index].agreePollId = opt.id;
								}else{
									tmpOpins[index].disAgreePollId = opt.id;
								}
							});
						}
						//$scope.resultSearch[index].description = $sce.trustAsHtml(opinion.description.split('&lt;').join('<').split('&gt;').join('>'), '<a href="$1">$1</a>');
					});
					
					$scope.opinions = tmpOpins;
					
					if($.isArray(tmpOpins[0])){
						$scope.opinions = tmpOpins[0];
							
					}
					$scope.pageData = tmpOpins;
					$scope.orderReverseData = tmpOpins;
//					$scope.resultSearch = tmpOpins;
					$scope.totalPages = Math.ceil($scope.pageData.length
							/ $scope.pageSize);
					$('#hola').hide();
					
				}else{
					$scope.opinions = [];
				}
			});
		};
		
		
		//Pagination start
		$scope.pageButtonDisabled = function(dir) {
			if (dir == -1) {
				return $scope.currentPage == 0;
			}
			return $scope.currentPage >= $scope.pageData.length
				/ $scope.pageSize - 1;
		}

		$scope.paginate = function(nextPrevMultiplier) {
			$scope.currentPage += (nextPrevMultiplier * 1);
			$scope.opinions = $scope.pageData
				.slice($scope.currentPage
					* $scope.pageSize);

		}

		$scope.shortOrderAccording = function(ord) {
			$scope.resultSearch = $scope.orderReverseData
				.slice().reverse();
			$scope.orderReverseData = $scope.resultSearch;
			$scope.pageData = $scope.resultSearch;
			console
				.log(" pageData :  - "
					+ $scope.pageData);
			console.log(" resultSearch :  - "
				+ $scope.resultSearch);
			$scope.currentPage = 0;
			// $scope.resultSearch = $scope.pageData
			// .slice(1 * $scope.pageSize);

//						console.log(" pageData After pagination:  - "
//								+ $scope.pageData);
//						console.log("length: --"
//								+ $scope.pageData.length);

			if ($scope.btnReverseStatus) {
				$scope.btnReverseStatus = false;
				$scope.OrderStatusbtn = 'New To Old'
			} else {
				$scope.btnReverseStatus = true;
				$scope.OrderStatusbtn = 'Old To New'
			}
		}
		
		//Pagination end
		
		
		
		//Load all the opinion only if the sentiment is selected
		if($scope.opinionSearchKey != null){
			getOpinion($scope.opinionSearchKey, true, false);
		}
		else if($scope.opinionViewLatest==true){
			getOpinion($scope.opinionSearchKey, false, true);
		}
		else if($scope.sentimentIdParam != 0){
			getOpinion($scope.sentimentIdParam, false, false);
		}
		
		$scope.searchOpinion = function(){
			if($scope.opinionSearchKey.length>2){
				if($scope.opinionSearchKey != null){
					$location.url("/viewAllOpinion?search="+$scope.opinionSearchKey);
				}else{
					$scope.opinionSearchKeyError = true;
				}
			}
		}
		
		$scope.displayOpinions = function(type, sentimentId){
			$('#hola').show();
			$location.search().search = null; // reset the search value
			$scope.opinionSearchKey = null;
			senIdSelectedForSearch =sentimentId;
			console.log('$scope.senIdSelectedForSearch ::: ', senIdSelectedForSearch);
			$( "#carousel-viewall .item ul li .box_sentiment" ).removeClass('active');
			$( "#carousel-viewall-perm .item ul li .box_sentiment" ).removeClass('active');
			$(type+sentimentId).addClass('active');
			getOpinion(sentimentId, false, false);
			
		}
		
		$scope.viewOpinion = function(opinionId){
			$location.url('/viewOpinion?id='+opinionId);
		};
		
		$scope.checkAuth = function(){
			if($scope.isUserLoggedIn() == true){  
			}else{
				$scope.loginNow();
			}
		}
		
		// Pinning Functionalitty
		document.getElementById('pinningOpinion').addEventListener('tribute-replaced', function (e) {
			if($scope.isTrend === true){
				$http({
					method: 'POST',
					url: $rootScope.liveHost+"/trend/createTrend",
					headers: {'Content-Type': 'application/json'},
					data:{"name":$scope.newTrend}
				}).then(function successCallback(response) {
					if(response.data.success === true){
						$scope.trendMapingIds["#"+$scope.newTrend]= response.data.messages[0].id;
						$scope.isTrend = false;
						$("#pinningOpinion").find("a.trend0").attr("href","/#/trends?id="+response.data.messages[0].id);
						$("#pinningOpinion").find("a.trend0").attr("class","trend"+response.data.messages[0].id);
						console.log("success: created new trend");
					}else{
						$scope.isTrend = false;
						console.log("succes : unable to create trend");
					}
				}, function errorCallback(response) {
					$scope.isTrend = false;
					console.log("unable to create trend");
				});
			}

		});
		
		
  }]).directive('callback', function() {
  return function(scope, element, attrs) {        
    setTimeout(function doWork(){
      //jquery code and plugins
	  if(attrs.sntmId == scope.sentimentIdParam){
		$(element).addClass('active');
	  }
    }, 0);        
  };
}).directive('tooltipCalback', function() {
  return function(scope, element, attrs) {        
    setTimeout(function doWork(){
      //jquery code and plugins
	  if(attrs.sntmId == scope.sentimentIdParam){
		$(element).tooltip();
	  }
    }, 0);        
  };
});

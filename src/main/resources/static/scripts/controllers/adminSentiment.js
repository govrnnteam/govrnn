'use strict';

var app = angular.module("govrnnAddSentiment", ['ngCookies', 'ngStorage']);
window.govrnnApp=app;
app.controller("adminAddSentimentCntrl", function ($scope, $http, $rootScope, $window, $location, $cookies, $localStorage) {
	$scope.SelectedCategories=[];
	$scope.categories = [
		    {id: 1, name: 'Agriculture & allied activities'},
		    {id: 2, name: 'Labour & Employment'},
		    {id: 3, name: 'Commerce & Trade'},
		    {id: 4, name: 'FDI'},
		    {id: 5, name: 'Taxation'},
		    {id: 6, name: 'Banking'},
		    {id: 7, name: 'Inflation'},
		    
		  ];
	$scope.toggle=function(id){
		if(SelectedCategories.length>0){
			for(var a=0;a<SelectedCategories.length;a++){
				if(SelectedCategories[a]==id)
					{
					SelectedCategories.pop(id);
					return
					}
				
			}
			SelectedCategories.push(id);
			alert(SelectedCategories.length);
			return;
		}
		
	};
	$scope.addSentiment = function() {
			var f = document.getElementById('avatar4').files[0],
			r = new FileReader();

			r.onloadend = function(e) {
				var image = e.target.result;
				$scope.image=image;
				$scope.$apply();
				var arr = image.split(",");
				var arr1 = arr[0].split(";");
				var arr2 = arr1[0].split(":"); 
				console.log(arr[1]);
				uploadFile({'imageByte':arr[1],'subject':$scope.sentiment.title,"description": $scope.sentiment.description,
				"keywords": $scope.sentiment.keywords,"imageType":arr2[1],"type": $scope.sentiment.type, "categories":$scope.sentiment.categories});
				//send your binary data via $http or $resource or do anything else with it
			}
			r.readAsDataURL(f);
		};
		
		
		
		function uploadFile(data){
			debugger;
			 $http({
	            url: '/admin/sentiment/add',
	            method: "POST",
	            data: data,
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(res){
	        	console.log(res);
	        },function(err){
	        	console.log('err',err);
	        })
	   }
	
});

app.controller("adminViewSentimentCntrl", function ($scope, $http, $rootScope, $window, $location) {
	console.log("You clicked submit!");
	$scope.getAllSentiment = function(type) {
		$http({
            url: '/admin/sentiment/getAllSentiments?sentimentType='+type,
            method: "GET",
            headers: {'Content-Type': 'application/json'}
        }).then(function(res){
        	$scope.sentiments=res.data.messages;
        	
        	for(var child=0;child<$scope.sentiments.length;child++ )
        	{
        		$scope.sentiments[child].isChecked=$scope.sentiments[child].state=='Alive'?true:false;
        	}
        },function(err){
        	console.log('err',err);
        })
	};
	$scope.sentClick=function(type){
		debugger
		$('#'+type).click();
		$scope.getAllSentiment(type);
	}

	$scope.EditSentiment=function(data){
		debugger;
		var postData={
				"id":data.id,
				"imageByte":data.imageByte,
				'subject':"manoj",
				"description": data.description,
				"keywords": data.keywords,
				"type": data.type,
				"categories":data.categories
				
		};
		
		debugger;
			 $http({
	            url: '/admin/sentiment/edit',
	            method: "POST",
	            data: postData,
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(res){
	        	console.log(res);
	        },function(err){
	        	console.log('err',err);
	        })
		alert(id);
			 
	};
	$scope.sentClick('temporary');
	$scope.updateState=function(data){
		data.state=data.isChecked==true?'Hidden':'Alive';
		debugger;
			 $http({
	            url: '/admin/disableEnableSentiment?state='+data.state,
	            method: "GET",
	           // data: data,
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(res){
	        	console.log(res);
	        },function(err){
	        	console.log('err',err);
	        })
		alert(id);
			 
	};
});
	

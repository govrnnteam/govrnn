'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description # MainCtrl Controller of the govrnnApp
 */
angular
	.module('govrnnApp')
	.controller(
		'PersonalityCtrl',
		[
			'$http',
			'$scope',
			'$rootScope',
			'CreatePersonalityService',
			'HttpHelper',
			'$location',
			'$localStorage',
			function($http, $scope, $rootScope,
					 CreatePersonalityService, HttpHelper,$location,$localStorage) {
				$scope.$storage = $localStorage;	
				var userIdParam = $location.search().id;
				$scope.id = userIdParam;
				$http.get('/user/find?id=' + $scope.id)
					.then(function(response) {
						if (response.data.messages != null) {
							//debugger;
							$scope.user = response.data.messages[0];
							$scope.uDate=new Date($scope.user.dob)
							// console.log($scope.opinion);
							// $scope.polls=$scope.opinion.sentiment.polls;
//													alert(JSOG
//															.stringify($scope.polls));

						}
					});

				$http.get('/opinion/getOpinionCountByUserId?id=' + $scope.id)
					.then(function(response) {
						if (response.data.messages != null) {
							$scope.opinionCount = response.data.messages[0];
							// console.log($scope.opinion);
							// $scope.polls=$scope.opinion.sentiment.polls;
//											alert(JSOG
//													.stringify($scope.polls));

						}
					});


				$http.get('/response/getCountByUserId?id=' + $scope.id)
					.then(function(response) {
						if (response.data.messages != null) {
							$scope.responsesCount = response.data.messages[0];

						}
					});

				$http.get('/opinion/getPastOpinionOutputModel?id=' + $scope.id)
					.then(function(response) {
						if (response.data.messages != null) {
							$scope.pastOpinionOutputModel = response.data.messages[0];
							// $scope.polls=$scope.opinion.sentiment.polls;
//											alert(JSOG
//													.stringify($scope.polls));

						}
					});
				
/*=============================================COVER PHOTO SECTION====================================================================*/				
				 $http.get('/user/downloadUserImage?userId='+$scope.id+'&imageType=albumimage&imageSize=standard').then(function(response) {
						if (response != null) {
							var coverImgval=response.data;
							//alert(JSOG.stringify(response));
							if(!response.data){$('#cover_img').attr('src','images/personality.png');}
							else{
								$('#cover_img').attr('src','data:image/png;base64,'+coverImgval);
								$('.banner_perso').css('background-image','url(' +coverImgval+ ')');
							}
				}
				 }); 
				 
				 $http.get('/user/downloadUserImage?userId='+$scope.id+'&imageType=userimage&imageSize=standard').then(function(response) {
						if (response != null) {
							$('#hola').hide();
							var userImgval=response.data;
							//alert(JSOG.stringify(response));
							if(!response.data){//$('#cover_img').attr('src','images/personality.png');
								
							}
							else{
								$('#per_user_img').attr('src','data:image/png;base64,'+userImgval);
								//$('.banner_perso').css('background-image','url(' +coverImgval+ ')');
							}
				}
				 }); 
				 
				 $scope.showCoverImage=function(){
						$('#uploadCoverFile').appendTo("body").modal('show');
						};
				$scope.uploadCoverFile = function(element) { 	  
					    	  if (typeof ($("#uploadCoverBtn")[0].files) != "undefined") {
					                var size = parseFloat($("#uploadCoverBtn")[0].files[0].size / 2048).toFixed(2);
					                var img_mb= size/1000;
					               // var size = parseFloat($("#uploadBtn")[0].files[0].size /2097152);
					               // alert(img_mb + "MB.");
					                if(img_mb<=2){
					                	$('#fileCoverUp').css("background", "green");
					                	//alert("good");
					                	//$scope.isExceeded = false;
					                	$('#uploadCoverFile').modal('hide');
					                	var regFormData = new FormData();
					                	regFormData.append('image',$(element)[0].files[0]);
					                	regFormData.append('imageType','albumimage'); 
					                	regFormData.append('userId',$scope.id);
					                	//alert(JSOG.stringify(regFormData));
					                	$http({
									      method: 'POST',
									      url: "/user/uploadimage",
									      headers: {'Content-Type': undefined},
									      data: regFormData
									      }).then(function successCallback(response) {
									     // alert(JSOG.stringify(response));
									      //location.reload();
									    	  $http.get('/user/downloadUserImage?userId='+$scope.id+'&imageType=albumimage&imageSize=standard').then(function(response) {
													if (response != null) {
														//alert(JSOG.stringify(response.data));
														var imgval= response.data;
														$('#cover_img').attr('src','data:image/png;base64,'+imgval);
														alert('Cover photo upated successfully.');														
													}
												});  

									      }, function errorCallback(response) {
									      alert("fail");
									      });
					                }else{
					                	$scope.errormsg = "please choose file upto 2MB size";
					                	$('#fileCoverUp').css("background", "red");
					                	alert("please choose file upto 2kb size");
					                		
					                }
					                
					            } else {
					                alert("This browser does not support HTML5.");
					            }
					    	  
					    	 // alert(JSOG.stringify($(element)[0].files[0]));
					    	  
					    	  
					    	  };
						 
/*===========================================user image section===================================================*/
				$scope.showImage=function(){
					$('#uploadFile').appendTo("body").modal('show');
					};
		      
					 $scope.showUserImage= function(){
				    	  $('#userPerPic_Modal').appendTo("body").modal('show');
				      }
				      
				     // $scope.attachment;		     
				      $scope.uploadFile = function(element) { 
				    	  
				    	  if (typeof ($("#uploadBtn")[0].files) != "undefined") {
				                var size = parseFloat($("#uploadBtn")[0].files[0].size / 2048).toFixed(2);
				                var img_mb= size/1000;
				               // var size = parseFloat($("#uploadBtn")[0].files[0].size /2097152);
				                //alert(img_mb + "MB.");
				                if(img_mb<=2){
				                	$('#fileUp').css("background", "green");
				                	//alert("good");
				                	//$scope.isExceeded = false;
				                	$('#uploadFile').modal('hide');
				                	var regFormData = new FormData();
				                	regFormData.append('image',$(element)[0].files[0]);
				                	regFormData.append('imageType','userimage'); 
				                	regFormData.append('userId',$scope.id);
				                	//alert(JSOG.stringify(regFormData));
				                	$http({
								      method: 'POST',
								      url: "/user/uploadimage",
								      headers: {'Content-Type': undefined},
								      data: regFormData
								      }).then(function successCallback(response) {
								     // alert(JSOG.stringify(response));
								      //location.reload();
								    	  $http.get('/user/downloadUserImage?userId='+$scope.id+'&imageType=userimage&imageSize=standard').then(function(response) {
												if (response != null) {
													//alert(JSOG.stringify(response.data));
													var imgval= response.data;
													$('#per_user_img').attr('src','data:image/png;base64,'+imgval);
													$('#indx_user_img').attr('src','data:image/png;base64,'+imgval);
													$('#per_usrModalImg').attr('src','data:image/png;base64,'+imgval);
													$('#userPic_Modal_img').attr('src','data:image/png;base64,'+imgval);
													$scope.$storage.sessionData.user_image = imgval;
													alert('Profile photo upated successfully.');
												}
											});  

								      }, function errorCallback(response) {
								      alert("fail");
								      });
				                }else{
				                	$scope.errormsg = "please choose file upto 2MB size";
				                	$('#fileUp').css("background", "red");
				                	alert("please choose file upto 2kb size");
				                		
				                }
				                
				            } else {
				                alert("This browser does not support HTML5.");
				            }
				    	  
				    	 // alert(JSOG.stringify($(element)[0].files[0]));
				    	  
				    	  
				    	  };
				    	  $scope.editorEnabled = false;
				    	  $scope.enableEditor = function() {
				    		    $scope.editorEnabled = true;
				    		    $scope.editableTitle = $scope.user.summary;
				    		  };
				    		  

				    		  $scope.disableEditor = function() {
				    		    $scope.editorEnabled = false;
				    		  };
				    		  

				    		  $scope.save = function() {
				    			$scope.user.summary = $scope.editableTitle;
				    			  var regFormData = new FormData();
					    		  regFormData.append('summary',$scope.user.summary);
					    		  regFormData.append('id',$scope.user.id);		  
					    		 
					    		  
					    		  $http({
					    	            method: 'POST',
					    	            url:"/user/updateAboutYou",
					    	            // headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					    	            headers: {'Content-Type': undefined},
					    	            // withCredentials: true,
					    	            data: regFormData
					    	          });
				    		    $scope.disableEditor();
				    		    return false;
				    		  };
				    		  
				    		  $('#updateInfo_personality').click(function(){
				    			  location.href = "/#/userSetting/id={{userData[0].id}}&tabNumber=1";
				    		  });
				    		  
				    		  
				    		  ///////Updated by manoj Gupta
				    		 
				    			  $scope.editUserEducation = function() {
					    			  

										var postData1={
											"id": $scope.user.id,
											"qualification": $scope.user.qualification
											
										};
										
										$http({
										      method: 'POST',
										      url: "/user/editUserEducation",
										      headers: {'Content-Type': 'application/json'},
										      data: postData1
										      }).then(function successCallback(response) {
										    	 $scope.isEdu=true;
										    	 alert('Education Details update successfully.');
										      });
					    		 
									
					    		  };
				    		  $scope.facebook = function() {
				    			  window.open($scope.user.faceBookEmail,'_blank');
					    		  };

					    	 $scope.googlePlus = function() {
					    		 window.open($scope.user.googlePlusEmail,'_blank');
						    	  };

						    $scope.twitter = function() {
						    	window.open($scope.user.twitterEmail,'_blank');
							};
							
							
						//////Added by manoj gupta 19 Aug
							$scope.updateContact=function(){
								
								var postData={
									"mobileNumber": $scope.user.mobileNumber,
									"emailId": $scope.user.emailId,
									"city": $scope.user.city,
									"state": $scope.user.state,
									"dob" :$scope.uDate
								};
								
								$http({
								      method: 'POST',
								      url: "user/editUserContact?id="+$scope.id,
								      headers: {'Content-Type': 'application/json'},
								      data: postData
								      }).then(function successCallback(response) {
								    	 $scope.isEdit=false;
								    	 alert('Contact Details update successfully.');
								      });
							};
							
							////Added by manoj for Past Opinions
							
							$scope.pastOpinions=function(u,m,t)
							{
								var month=m.split('-')[0];
								if(month=="Jan")
							         month="January"
							        else if(month=="Feb")
							         month="February"
							        else if(month=="Mar")
							         month="March"
							        else if(month=="Apr")
							         month="April"
							        else if(month=="May")
							         month="May"
							        else if(month=="Jun")
							         month="June"
							        else if(month=="Jul")
							         month="July"
							        else if(month=="Aug")
							         month="August"
							        else if(month=="Sep")
							         month="September"
							        else if(month=="Oct")
							         month="October"
							        	    else if(month=="Nov")
							        	         month="November"
							        	        else if(month=="Dec")
							        	         month="December"
								var year=m.split('-')[1];
								$("#hola").show();
								$http.get("user/opinionDetails?userId="+u+"&month="+month+"&year="+year+"&opinionType="+t).then(function(response) {
									if (response.data.messages != null) {
										$("#pastOpinionDiv").show();
										$scope._pastOpinions=response.data.messages;
										 $('#hola').hide();
																							
									}
									else
										{
										$("#pastOpinionDiv").hide();
										 $('#hola').hide();
										}
								});  
														
							};
							
						
							
			} ]);

'use strict';

var app = angular.module("govrnnAppAdmin", ['ngCookies', 'ngStorage']);
app.controller("loginAdminCtrl", function ($scope, $http, $rootScope, $window, $location, $cookies, $localStorage) {
	$scope.authenticateAdmin = function (form) {
		console.log("You clicked submit!");
		console.log("$rootScope.liveHost::", $rootScope.liveHost);
		var loginData;
		if (form.$valid) {
			loginData = $.param({
				name: $scope.user.name,
				password: $scope.user.password
			});
			console.log(loginData);
			$http({
				method: 'POST',
				url: "/admin/authenticateAdmin",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: loginData
			}).then(function successCallback(response) {
				if (response.data.success === true) {
					console.log('sucess : ', response.data);
					$scope.$storage = $localStorage;
					angular.forEach(response.data.messages, function (value, key) {
						$cookies.putObject('userID', value.id);

						localStorage.setItem('AuthData', JSON.stringify(value));

						$scope.$storage.sessionData = {};
						$scope.$storage.sessionData.isLoggedIn = true;
						$scope.$storage.sessionData.user_name = value.name;
						$scope.$storage.sessionData.user_id = value.id;
						console.log("userData :", JSON.parse(localStorage.getItem('AuthData')));

						$window.location.href = '/dashboard.html';
					});


				} else {
					$scope.hasError = true;
					$scope.user = {};
					form.$setPristine();
					console.log("succes:error login");
				}
			});
		}

	}
});

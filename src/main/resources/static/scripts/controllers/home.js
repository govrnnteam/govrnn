'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
	.controller('HomeCtrl', ['$http', '$scope', '$rootScope', '$routeParams','SentimentService', 'HomeService','$sce','$interval','$window',
		function ($http, $scope, $rootScope, $routeParams, SentimentService, HomeService,$sce,$interval,$window) {
		 $( ".show_onclick" ).show();
			$( ".collapse_sec" ).hide();		
			$rootScope.id = 0,
			$scope.newsFlash = "Gulberg massacre verdict: 11 awarded life, 12 others to be jailed";
			$scope.tab = 1;
			$scope.emojiTab=0;
			$scope.clickedtab;
			var sentiments= $scope.sentiments = [];
			$scope.selectedSentiment = {};
			$scope.isSentimentClicked= false;
			$scope.isRelatedClicked=false;
			$scope.ifRelSentiment=true;
			$scope.isAggregationPresent=false;
			$scope.rateSentiment= {};
			$scope.temporarySentimentsSets = [];
			$scope.permanentSentimentsSets = [];
			$scope.relatedSentimentsSets=[];
			$scope.pollSets=[];
			$scope.rateNews= {};
			$scope.rateRef= {};
			$scope.refValidation="";
			$scope.newsValidation="";
			$scope.searchValue="";
			$scope.searchValues="";
			$scope.opinionOutputModelsSets =[];
			$scope.sentimentCauroselMinLength = 0;
			$scope.sentimentCauroselMaxLength = 8;
			$scope.totalVotes = 0 ;

			$scope.tempSentimentCaurselFrom= $scope.sentimentCauroselMinLength ;
			$scope.permSentimentCaurselFrom = $scope.sentimentCauroselMinLength ;
			
			$scope.tempSentimentCaurselTo = $scope.sentimentCauroselMaxLength ;
			$scope.permSentimentCaurselTo= $scope.sentimentCauroselMaxLength ;
			/*$scope.loggedUserId=$scope.getLoggedInUserId();*/
				
			$http.get("/opinion/getLatestOpinions?count=3").then(function(response){
$("#hola").hide();
				  if(response.data.messages != null){
					    var opinionOutputModels = response.data.messages[0];
					    for(var i =0;i<response.data.messages[0].length;i++){
					    	opinionOutputModels[i].opinionText = $sce.trustAsHtml(opinionOutputModels[i].text);
							$scope.opinionOutputModelsSets .push(opinionOutputModels[i]);
					    }
				  }
			    });
				
				$http.get("/user/getLatestUsers?count=5").then(function(response){
					  if(response.data.messages != null){
						    $scope.latestUsers = response.data.messages[0];
						    
						    $("#carousel-latestuser").carousel({interval: 3000});   
					  }
				  });
			  
				$scope.topTrendsCounter = 0;
				var count = 0;
				$scope.topAdminTrends =[];
				var startTopTrendLoop = function(){
				    $interval(function(){
				    	if($scope.lcarousel-inneratestTrends.length != 0 && $scope.topTrendsCounter < $scope.latestTrends.length ){
				    		$scope.topAdminTrends =[];
				    		$scope.topTrendsCounter  +=8;
				    		for (var i =  $scope.topTrendsCounter-8 ; i <$scope.topTrendsCounter; i++) {
								if(i < $scope.latestTrends.length){
									var trendObj =  $scope.latestTrends[i];									
									$scope.topAdminTrends.push(trendObj);
					    		}	
								else{
									$scope.topTrendsCounter = 0;									
								}
							 }
				     	}
				    	else if ($scope.topTrendsCounter  > $scope.latestTrends.length ){
				    		$scope.topTrendsCounter = 0;
				    	}
				    	else if ($scope.topAdminTrends.lenght == 0){
				    		$scope.topAdminTrends = $scope.latestTrends;
				    	}
				    },5000);
				}
				
			  $http
				.get('/trend/getTopAdminTrends?count=100')
				.then(function(response) {
					if (response.data.success) {
						$scope.latestTrends = response.data.messages[0];
						for (var i = 0; i < $scope.latestTrends.length; i++) {
							$scope.latestTrends[i].title = '#'+$scope.latestTrends[i].title;
						}
						startTopTrendLoop();
						//$scope.topTrends = $scope.latestTrends;
					}
				});
			//====================GET TOP TRENDS=====================================		
			$scope.topTrends = function(count) {
				$http
					.get('/trend/getTopTrends?count='+count)
					.then(
						function(response) {
							if (response.data.success) {
								$scope.topTrends = response.data.messages[0];
								for (var i = 0; i < $scope.topTrends.length; i++) {
									//console.log(topTrends[i].title);
								}
							}
						});
			};

			//====================GET NEWS=====================================		
			  $http
				.get('/dailyNews/getDailyNews')
				.then(function(response) {
					if (response.data.success) {
						$scope.dailyNews = response.data.messages[0];
						for (var i = 0; i < $scope.dailyNews.length; i++) {
							$scope.dailyNews[i].title = $scope.dailyNews[i].title;
						}
					}
				});
			
			/*=====================================RATING APIS==============================================*/

			$scope.rate= function(componentType, attrid, parentId, rate, key,feelType){
				if($scope.isUserLoggedIn() == true){
				$scope.rateSentiment.componentType= componentType;
				$scope.rateSentiment.componentId= attrid;
				$scope.rateSentiment.parentId= parentId;
				$scope.rateSentiment.rating = rate;
				$scope.rateSentiment.feelType = feelType;
				$scope.rateSentiment.userId = $scope.getLoggedInUserId();
				
				if(componentType=="reference"){

					$scope.rateRef.componentType= "reference";
					$scope.rateRef.parentId= parentId;
					$scope.rateRef.userId = $scope.getLoggedInUserId();;

					$http({
						method: 'POST',
						url: "/rating/getRating",
						headers: {'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data:  JSON.stringify($scope.rateRef)
					}).then(function successCallback(response) {
						$scope.refList=response.data.messages[0];
						alert(JSON.stringify(response.data.messages[0]));
						if($scope.refList.length != 0){
							for(var i=0; i<$scope.refList.length;i++){
								if($scope.refList[i].componentId == $scope.rateSentiment.componentId && $scope.refList[i].rating == $scope.rateSentiment.rating){
									//$('#rate_'+rate+attrid).css("background", "");
									$scope.refValidation=0;
									//alert("matched"+$scope.refValidation);
									break;
								}else{
									$scope.refValidation=1;
								}
							}}else{
							$scope.refValidation=2; 	
						}

						if($scope.refValidation>0){

							HomeService.saveRating($scope.rateSentiment);
							var path = "/images/"+key+".png";
							var imgId="refImg_"+attrid+"_"+key;
                            // Get app child images inside div
							var tempcount = 0;
							var DivId = "#refPar_smiley_sakir_"+attrid +' img';
							 $(DivId).each(function () {
								 tempcount++;
								 var tempId= $(this).attr('src');
								 var tempFeelType =$(this).attr('data-FeelType');
								 var tempKey = $(this).attr('data-key');
								 
								 // This feel type already exist, then replace
								 if(tempFeelType == feelType){
									// alert('Same feel');
									 $(this).attr('src',path);
									 $(this).attr('data-FeelType',feelType);
									 $(this).attr('data-key',key);
									 $(this).attr('id',imgId);
									 return false;
								}
								 else{
									 var isExist = false;
									 $(DivId).each(function () {
										 var ttfeel = $(this).attr('data-FeelType');
										 
										 if(ttfeel == feelType)
											 {
											 $(this).attr('src',path);
											 $(this).attr('data-FeelType',feelType);
											 $(this).attr('data-key',key);
											 $(this).attr('id',imgId);
											 isExist =true;
											 return false;
											 }
									 });
									 if(isExist == false)
									 {
									// alert("this is 1st feel of this type");
										 $("#refImg_"+attrid).css('display', 'none');
									 $("#refPar_smiley_sakir_"+attrid).append("<img src='"+path+"' id='"+imgId+"' data-FeelType='"+feelType+"' data-key='"+key+"'>");
								      return false;
								     }
								 }
						});
						
							 if(tempcount == 0){
								 //alert("This is 1st item in div");
								 $("#refImg_"+attrid).css('display', 'none');
								 $("#refPar_smiley_sakir_"+attrid).append("<img src='"+path+"' id='"+imgId+"' data-FeelType='"+feelType+"' data-key='"+key+"'>");
							 }
							
							//$('#rate_'+rate+attrid).css("background", "grey");
							//$("#refImg_"+attrid).attr("src","/images/"+key+".png");
							//alert("saveRating-1");
							//$("#refImg_"+attrid+1).attr("src","/images/"+attractive+".png");
							//alert("saveRating-2");
						}else{
							//alert($scope.refValidation);
							$('#rate_'+rate+attrid).css("background", "");
							$("#refImg_"+attrid).attr("src","/images/feel.png");//smiley.jpg
							HomeService.deleteRating($scope.rateSentiment);
						}

					}, function errorCallback(response) {
						console.log(response);
						return {};

					});

				}
				else{
					$scope.rateNews.componentType= "sentimentNews";
					$scope.rateNews.parentId= parentId;
					$scope.rateNews.userId = $scope.getLoggedInUserId();;

					$http({
						method: 'POST',
						url: "/rating/getRating",
						headers: {'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data:  JSON.stringify($scope.rateNews)
					}).then(function successCallback(response) {
						$scope.newsList= response.data.messages[0];
						if($scope.newsList.length != 0){
							for(var i=0; i<$scope.newsList.length;i++){
								if($scope.newsList[i].componentId == $scope.rateSentiment.componentId && $scope.newsList[i].rating == $scope.rateSentiment.rating){
									$scope.newsValidation=0;
									break;

								}else{
									$scope.newsValidation=1;
								}
							}}else{
							$scope.newsValidation=2;
						}

						if($scope.newsValidation>0){

							$("#newsImg_"+attrid).attr("src","/images/"+key+".png");
							HomeService.saveRating($scope.rateSentiment);
						}else{
							//alert($scope.newsValidation);
							$('#rate_'+rate+attrid).css("background", "");
							$("#newsImg_"+attrid).attr("src","/images/feel.png");//smiley-1
							//HomeService.deleteRating($scope.rateSentiment);
						}
					}, function errorCallback(response) {
						console.log(response);
						return {};

					});
				}
				}else{
					$scope.loginNow();
				}
			};
			/*$http.get('/poll/getPollAggregationBean?pollId=7').then(
			 function(response){
			 $scope.voteResp = response.data.messages[0];
			 //alert();
			 for(var i=0; i<$scope.voteResp.length;i++)
			 alert($scope.voteResp[i].percentage);
			 });*/
			$scope.navClick=function(){
				alert("This feature is coming soon!");
				return false;
			}
			$scope.featureComingSoon=function (){
				alert("This feature is coming soon!");
				return false;
			}

			//====================DEFAULT POLL VIEW(AS PER USER PREV SESSION)=====================================		
			$scope.pollAnsAsPerUser= function(id, polls){
				if($scope.isUserLoggedIn() == true){
				$http.get('/poll/getAllPollOptionBySentimentBean?sentimentId='+id+'&userId='+$scope.getLoggedInUserId()).then(
				function(response){
					$scope.pollResp = response.data.messages[0];
					for(var i=0; i<$scope.pollResp.length;i++){
							if($scope.pollResp[i].pollOptionId){
								$('#poll_'+$scope.pollResp[i].pollOptionId).attr('checked','true');
								$scope.pollAgrregation($scope.pollResp[i].pollId);
							}
						}
					});
				}
			};

			//$scope.lineShow=false;
			/*======================================================POLL VOTE API========================================	*/
		
			$scope.pollAgrregation=function(pollId){
				//alert("inside poll aggregation"+pollId);
				$http.get('/poll/getPollAggregationBean?pollId='+pollId).then(
					function(response){
						$scope.voteResp = response.data.messages[0];
						for(var i=0; i<$scope.voteResp.length; i++){
							$('#agre_'+$scope.voteResp[i].pollOptionId+"_"+pollId).css("display","block");
							$('#agre_'+$scope.voteResp[i].pollOptionId+"_"+pollId).text($scope.voteResp[i].percentage+'%');
							var redper=$scope.voteResp[i].percentage;
							var tran=(100-$scope.voteResp[i].percentage);
							$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("display","block");
							if(redper>0){
							$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("background","linear-gradient(90deg, red "+redper+"%, transparent "+tran+"%)");
							}else{
								$('#pollOpn_'+$scope.voteResp[i].pollOptionId).css("background","transparent 100%");
							}
						}
					});
			};
			/*	=========================VOTE API================================================*/
			$scope.vote= function(pollId,pollOptnId,compType){
				//alert($scope.getLoggedInUserId());
				if($scope.isUserLoggedIn() == true && $scope.getLoggedInUserId()!='null'){
				$http.get('/poll/getPollOption?pollId='+pollId+'&userId='+$scope.getLoggedInUserId()).then(
					function(response){
						//alert("inside"+$scope.voteRes);
						$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
						$scope.totalVotes += 1;
					if($scope.voteRes){
						if(pollOptnId!=$scope.voteRes){
							$scope.unSelectVote($scope.voteRes, pollId);
							$scope.selectVote(pollOptnId, compType, pollId );
						}else{
							//alert("already clicked");
							//$scope.selectVote(pollOptnId,compType, pollId );
						}//Long pollOptionId, Long userId
						}else if($scope.voteRes==""||$scope.voteRes==undefined){
							$scope.selectVote(pollOptnId,compType, pollId );
						}	//pollOptionId, userId, componentType, componentId
					});
			}
			else{
				$('#poll_'+pollOptnId).attr('checked',false);
				$scope.loginNow();
			}
		};
		
		$scope.selectVote= function(pollOptnId, compType, pollId ){
			$http.post('/poll/selectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.getLoggedInUserId() +'&componentType='+compType +'&componentId='+pollId).then(
					function(response){					
					$scope.voteResSelect = JSOG.parse(JSOG.stringify(response.data.messages[0]));
					$scope.pollAgrregation(pollId);
			});
		};

		$scope.unSelectVote= function(pollOptnId, pollId){
			$http.post('/poll/unSelectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.getLoggedInUserId()).then(
			function(response){
				$scope.voteResUnselect = JSOG.parse(JSOG.stringify(response.data.messages[0]));
				$scope.pollAgrregation(pollId);
			});
		};


		/*==========================GET ALL SENTIMENT==================================*/
			/*$http.get('/sentiment/getAll').then(
				function(response) {
					console.log('hello');
					$scope.sentiments = JSOG.parse(JSOG
						.stringify(response.data.messages));
				});
			*/


			/*=============================DISPLAY SENTIMENT -sentiment Description tab========================================*/

			$scope.isClicked = function(tabNum){
				return $scope.clickedtab === tabNum;
				//console.log($scope.clickedtab);
			};

			$scope.displaySentiment = function(newTab, id, sentiType){
				debugger;
				//for(var key in $scope.permanentSentiments){
				$("#permanent .box_sentiment.active").removeClass("active");//}
				for(var key in $scope.temporarySentiments){
				$("#current_"+key).removeClass("active");}
				$( "#carousel-sentiment .item ul li .box_sentiment" ).removeClass('active');
				$( "#carousel-sentiment-perm .item ul li .box_sentiment" ).removeClass('active');
				$('#'+sentiType+'_'+id).addClass('active');
                pauseSlides();
                $( ".show_onclick" ).show();
				$( ".collapse_sec" ).hide();
				$('html, body').animate({scrollTop:460}, 'slow');
				$http.get('/sentiment/findSentiment?id='+id).then(
					function(response) {
						if (response.data.messages != null) {
							$scope.selectedSentiment = response.data.messages[0];
							$scope.clickedtab = newTab;
							$scope.isSentimentClicked = true;
							$scope.aggregation(id);
							$scope.userRate(id);
							for(var j=0 ;j <  $scope.selectedSentiment.polls.length ; j++){
								$scope.totalVotes += $scope.selectedSentiment.polls[j].totalVotes;
							}							
							$scope.pollAnsAsPerUser(id, $scope.selectedSentiment.polls);
							$scope.references= $scope.selectedSentiment.attachments;
							$scope.relatedSentiments= $scope.selectedSentiment.relatedSentiments;
							var data = $scope.relatedSentiments;
							var relatedSentimentLength = data.length;

							for (var i = 0; i < relatedSentimentLength; i += 6) {
								var newobj = {};
								for(var j=i; j< (i+6); j++){
									if(data[j]){
										newobj[j]= data[j];
									}
								}
								$scope.relatedSentimentsSets.push(newobj);
								console.log($scope.relatedSentimentsSets);
								
							}
							var polId=response.data.messages[0].polls[0].id;
							$('#p_1_'+polId).css('background','red');
							$('#p_1_'+polId).css('transform','scale(1.3)');
							// $("#carousel-related").carousel({interval: 3000, pause: "hover"}); 
							// $("#carousel-poll").carousel({interval: 3000, pause: "hover"}); 
							//$window.scrollTo(0, 850);
						}
					});

			};

//===========================================DEFAULT RATE VIEW=======================================

			$scope.userRefRate={};
			$scope.userNewsRate={};
			$scope.userRate= function(id){
				if($scope.isUserLoggedIn() == true){
				$scope.userRefRate.componentType="reference";
				$scope.userRefRate.parentId = id;
				$scope.userRefRate.userId = $scope.getLoggedInUserId();
				HomeService.getAllRating($scope.userRefRate, "reference");
				
				$scope.userNewsRate.componentType="sentimentNews";
				$scope.userNewsRate.parentId = id;
				
				$scope.userNewsRate.userId = $scope.getLoggedInUserId();
				$scope.allNewsRating = HomeService.getAllRating($scope.userNewsRate, "sentimentNews");
				
				}
				//alert($scope.allRefRating);
			}

			/*$scope.highlight= function(allRating){
			 alert("came");
			 alert(allRating.length);
			 return;
			 }*/

//===============================================aggregation API=================================


			$scope.aggregation = function(id){
				//alert(id);
				$http.get('/opinion/getOpinionAggregation?sentimentId='+id).then(
					function(response){
						//if(response.data.success){
						$scope.aggregate = response.data.messages;
					
						if($scope.aggregate!=null){
						$scope.isAggregationPresent=false;
						$scope.totalOpinionVotes =$scope.aggregate[0].totalVotes; 
						$scope.supportPercentage=Math.round((($scope.aggregate[0].support)/($scope.aggregate[0].support+$scope.aggregate[0].against))*100);
						$scope.againstPercentage=Math.round((($scope.aggregate[0].against)/($scope.aggregate[0].support+$scope.aggregate[0].against))*100);
						$scope.myJson = {
							globals: {
								shadow: false,
								fontFamily: "Verdana",
								fontWeight: "100"
							},
							type: "pie",
							backgroundColor: "#fff",

							legend: {

								layout: "x5",
								position: "none",
								borderColor: "transparent",
								marker: {
									borderRadius: 10,
									borderColor: "transparent"
								}
							},
							tooltip: {
								text: "%v"+"%"+ "%t"
							},
							plot: {
								refAngle: "-90",
								borderWidth: "5px",
								valueBox: {
									placement: "in",
									text: "",
									fontSize: "15px",
									textAlpha: 1,
								}
							},
							series:[
								 {
									text: "against",
									values: [$scope.againstPercentage],
									backgroundColor: "#128807"
								},
								{
									text: "support",
									values: [$scope.supportPercentage],
									backgroundColor: "#ff7305",
								}
								]
						};
						}
						else {
							//alert("null");
							$scope.isAggregationPresent=true;
						}
					});
					};
				

			/*--------------------------------------tab section of current/permanent----------------------------------*/
			$scope.setTab = function(newTab){
				$scope.tab = newTab;
				 
				if($scope.tab == 2){
					loadPermanentSentiemnts();
				}
				$window.scrollTo(0, 450);  
			};

			$scope.isSet = function(tabNum){
				return $scope.tab === tabNum;
			};

			$scope.from = $scope.sentimentCauroselMinLength;;
			$scope.to = $scope.sentimentCauroselMaxLength;
			$scope.totalPermSentiemnts ;
			$scope.totalTempSentiemnts ;
				$http.get("/sentiment/getSentimentListSizeByType?type=Permanent").then(function(response){
					  if(response.data.messages != null){
						   $scope.totalPermSentiemnts  = response.data.messages[0];
						   
					  }
				    });


			$http.get("/sentiment/getSentimentListSizeByType?type=Temporary").then(function(response){
				  if(response.data.messages != null){
					  $scope.totalTempSentiemnts = response.data.messages[0];
					    
				  }
			    });
			/*=========================================permanent/current sentiment==========================================*/		
			$scope.PollPrev = function(slider, pollId, index, length, button){
				myService.GetNewsLanguage();
				if(button=='Next'){
					if(index+1 < length){
						var nextId =pollId+1;
						var divId = "#pollDivi_"+pollId;
						var nextDiv = "#pollDivi_"+nextId;
						$(divId).parent().removeClass('active');
						$(nextDiv).parent().addClass('active');
						$("#p_1_"+pollId).css({"background-color":"gray","transform":"scale(1.3)"});
						$("#p_1_"+nextId).css({"background-color":"red","transform":"scale(1.3)"});
						$("#btnnext_"+pollId).addClass("ng-hide");
						$("#btnnext_"+nextId).removeClass("ng-hide");
					}
				}
				else if(button=='Prev'){
					if(index > 0){
						var PrevePollId =pollId-1;
						var divId = "#pollDivi_"+pollId;
						var PreveDiv = "#pollDivi_"+PrevePollId;
						$(divId).parent().removeClass('active');
						$(PreveDiv).parent().addClass('active');
						$("#p_1_"+pollId).css({"background-color":"gray","transform":"scale(1.3)"});
						$("#p_1_"+PrevePollId).css({"background-color":"red","transform":"scale(1.3)"});
						
						     $("#btnnext_"+pollId).addClass("ng-hide");
						     $("#btnnext_"+PrevePollId).removeClass("ng-hide");
					}
				}
				
}
			
			
			$scope.prev = function(slide) {

				//$("#hola").show();
				if(slide == '#carousel-sentiment-perma'  )
				{

					if($scope.permSentimentCaurselTo != $scope.sentimentCauroselMaxLength){
						$scope.permSentimentCaurselFrom -= $scope.sentimentCauroselMaxLength ;						
						$scope.permSentimentCaurselTo -= $scope.sentimentCauroselMaxLength ;
					}					
				}
				else if(slide == '#carousel-sentiment'){
		
					if($scope.tempSentimentCaurselTo != $scope.sentimentCauroselMaxLength){
						$scope.tempSentimentCaurselFrom -= $scope.sentimentCauroselMaxLength ;						
						$scope.tempSentimentCaurselTo -= $scope.sentimentCauroselMaxLength ;

					}								
				}
				else if(slide == '#carousel-newlyjoined'){
					$scope.permSentimentCaurselFrom -= $scope.sentimentCauroselMaxLength ;						
					$scope.permSentimentCaurselTo -= $scope.sentimentCauroselMaxLength ;
				}
				$(slide).carousel("prev");

				//$("#hola").hide();
	      };
			
			$scope.next = function(slide) {	
			
				//$("#hola").show();
				if(slide =='#carousel-poll'){
					if($scope.permSentimentCaurselTo < $scope.totalPermSentiemnts){
						
						$scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo ;						
						$scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

						if($scope.permSentimentCaurselTo >  ($scope.permanentSentimentsSets.length * 8 ) ){
							
							SentimentService.findByTypePermanent($scope.permSentimentCaurselFrom ,$scope.permSentimentCaurselTo).then(function(response) {
								//console.log('called per');

								$scope.permanentSentiments = response;
								var data = response;
								var sentimentLength = data.length;
								
								for (var i = 0; i < sentimentLength; i += 8) {
									//console.log(i);
									var newobj = {};
									for(var j=i; j< (i+8); j++){
										if(data[j]){
											newobj[j]= data[j];
										}
									}
									$scope.permanentSentimentsSets.push(newobj);
								}
							
								$(slide).carousel("next");
							});

						}
						else{
						
							$(slide).carousel("next");
						}
					}
				
				}
				else if(slide == '#carousel-newlyjoined')
				{
					if($scope.permSentimentCaurselTo < $scope.totalPermSentiemnts){
				
						$scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo ;						
						$scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

						if($scope.permSentimentCaurselTo >  ($scope.permanentSentimentsSets.length * 8 ) ){
				
							SentimentService.findByTypePermanent($scope.permSentimentCaurselFrom ,$scope.permSentimentCaurselTo).then(function(response) {
								//console.log('called per');

								$scope.permanentSentiments = response;
								var data = response;
								var sentimentLength = data.length;
					
								for (var i = 0; i < sentimentLength; i += 8) {
									//console.log(i);
									var newobj = {};
									for(var j=i; j< (i+8); j++){
										if(data[j]){
											newobj[j]= data[j];
										}
									}
									$scope.permanentSentimentsSets.push(newobj);
								}

								$(slide).carousel("next");
							});

						}
						else{
							
							$(slide).carousel("next");
						}
					}
				}
				
				else if(slide == '#carousel-sentiment-perma'){				 
				
					if($scope.permSentimentCaurselTo < $scope.totalPermSentiemnts){
	
						$scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo ;						
						$scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

						if($scope.permSentimentCaurselTo >  ($scope.permanentSentimentsSets.length * 8 ) ){

							SentimentService.findByTypePermanent($scope.permSentimentCaurselFrom ,$scope.permSentimentCaurselTo).then(function(response) {
								//console.log('called per');

								$scope.permanentSentiments = response;
								var data = response;
								var sentimentLength = data.length;

								for (var i = 0; i < sentimentLength; i += 8) {
									//console.log(i);
									var newobj = {};
									for(var j=i; j< (i+8); j++){
										if(data[j]){
											newobj[j]= data[j];
										}
									}
									$scope.permanentSentimentsSets.push(newobj);
								}
							
								$(slide).carousel("next");
							});

						}
						else{
				
							$(slide).carousel("next");
						}
					}
				}else if(slide == '#carousel-sentiment'){
			
					if($scope.tempSentimentCaurselTo < $scope.totalTempSentiemnts){
	
						$scope.tempSentimentCaurselFrom = $scope.tempSentimentCaurselTo ;						
						$scope.tempSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;

						if($scope.tempSentimentCaurselTo >  ($scope.temporarySentimentsSets.length * 8 ) ){
		

							SentimentService.findByTypeTemporary($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo).then(function(response) {
								$scope.temporarySentiments = response;
								var data = response;
								var sentimentLength = data.length;

								for (var i = 0; i < sentimentLength; i += 8) {
									var newobj = {};
									for(var j=i; j< (i+8); j++){
										if(data[j]){
											newobj[j]= data[j];
										}
									}
									$scope.temporarySentimentsSets.push(newobj);
									//$("#hola").hide();
								}
					
								$(slide).carousel("next");
							});

						}else{
							
							$(slide).carousel("next");
						}
					}					
				
				}
			

			};
			
			/* Start function NextLoginUser -- Login user profile div Next button click 
			$scope.NextLoginUser = function(slide) {
				
				alert("NextLoginUser --- Next Button click - 1");
				alert("NextLoginUser --- Next Button click - 1"+slide:);
				if(slide == '#carousel-newlyjoined'){
					if($scope.permSentimentCaurselTo < $scope.totalPermSentiemnts){
						alert("Next Button click - 2");
						$scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo ;						
						$scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength ;
					
					
						if($scope.permSentimentCaurselTo >  ($scope.permanentSentimentsSets.length * 8 ) ){

							SentimentService.findByTypePermanent($scope.permSentimentCaurselFrom ,$scope.permSentimentCaurselTo).then(function(response) {
								//console.log('called per');

								$scope.permanentSentiments = response;
								var data = response;
								var sentimentLength = data.length;

								for (var i = 0; i < sentimentLength; i += 8) {
									//console.log(i);
									var newobj = {};
									for(var j=i; j< (i+8); j++){
										if(data[j]){
											newobj[j]= data[j];
										}
									}
									$scope.permanentSentimentsSets.push(newobj);
								}
								alert("End -9");
								$(slide).carousel("next");
							});

						}
						else{
							alert("Next Button click - Else-1");
							$(slide).carousel("next");
						}
					}
					
					
				}	
				
				
			};
			/* End of Function NextLoginUser*/
			

	/*		
			SentimentService.findByTypePermanent(from ,to).then(function(response) {
				//console.log('called per');

				$scope.permanentSentiments = response;
				var data = response;
				var sentimentLength = data.length;

				for (var i = 0; i < sentimentLength; i += 8) {
					//console.log(i);
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
						}
					}
					$scope.permanentSentimentsSets.push(newobj);
				}
				$("#carousel-sentiment-perma").carousel({interval: 3000, pause: "hover"});
				//$("#carousel-sentiment").carousel({interval: 3000, pause: "hover"});
			});

			SentimentService.findByTypeTemporary(from ,to).then(function(response) {
				$scope.temporarySentiments = response;
				var data = response;
				var sentimentLength = data.length;

				for (var i = 0; i < sentimentLength; i += 8) {
					var newobj = {};
					for(var j=i; j< (i+8); j++){
						if(data[j]){
							newobj[j]= data[j];
						}
					}
					$scope.temporarySentimentsSets.push(newobj);
				}
				$("#carousel-sentiment").carousel({interval: 5000, pause: "hover"});
			});
*/
//========================================relatedSentiment onClick event==================================================

			$scope.displayRelatedSentiment = function(newTab, id){
				$http.get('/sentiment/findSentiment?id='+id).then(
					function(response) {
						if (response.data.messages != null) {
							$scope.sentiment = response.data.messages[0];
							$scope.isSentimentClicked = true;
							var type= $scope.sentiment.type;
							
												
													//for(var key in $scope.permanentSentiments){
														$("#permanent .box_sentiment.active").removeClass("active");
																//}
													for(var key in $scope.temporarySentiments){
														$("#current_"+key).removeClass("active");}
													if(type=="Permanent"){
														$scope.displaySentiment(key, id, 'permanent');
													}
												else{
														$scope.displaySentiment(key, id, 'current' );
														
													}
												}
											
									});
	
						};
			
			
			
			/*===================================BY SENTIMENT ID IN THE URL========================================*/
			
			//$scope.temporarySentiments, $scope.permanentSentiments
						
						
			if($routeParams.sentimentId!=null){	
				debugger;
				$scope.sentitId=$routeParams.sentimentId;
				$http.get('/sentiment/findSentiment?id='+$scope.sentitId).then(
						function(response) {
							
							if (response.data.messages != null) {
								var type=response.data.messages[0].type;
								
									if(type=="Temporary"){
										setTemporarySentimentsSets( $scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo); 
										//setPermanentSentimentsSets($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
										
										//return $scope.temporarySentimentsSets;
									}
									else {
										setPermanentSentimentsSets($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
										//setTemporarySentimentsSets( $scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo); 
										
										//return $scope.permanentSentimentsSets;
									}								
									
							}
						});
			}
			else{		
				debugger;
				  $scope.tempSentimentCaurselFrom = $scope.sentimentCauroselMinLength;
				  $scope.tempSentimentCaurselTo = $scope.sentimentCauroselMaxLength;
				  
				  setTemporarySentimentsSets ($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
				  setPermanentSentimentsSets($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
				 
			}
			
			
			function loadPermanentSentiemnts(){
				
				if($scope.permanentSentimentsSets.length == 0){
					 $scope.permSentimentCaurselFrom = $scope.sentimentCauroselMinLength;
					  $scope.permSentimentCaurselTo = $scope.sentimentCauroselMaxLength;
					  
					  setPermanentSentimentsSets ($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
				
				}				 
			};
			
			$scope.sentimentFound = false;
			
			function setPermanentSentimentsSets (from,to){
				SentimentService.findByTypePermanent(from,to).then(function(response) {
					$scope.permanentSentiments = response;
					var data = response;
					var sentimentLength = data.length;
					if($routeParams.sentimentId == null && $routeParams.sentimentId == undefined){
						$scope.sentimentFound = true;						
					}
					for (var i = 0; i < sentimentLength; i += 8) {
						var newobj = {};
						for(var j=i; j< (i+8); j++){
							if(data[j]){
								newobj[j]= data[j]; 
								
								if($routeParams.sentimentId != null && $routeParams.sentimentId != undefined){
									if(data[j].id == $routeParams.sentimentId ){
										$scope.sentimentFound = true;
										$scope.tab=2;
										$("#permanent"+$scope.sentitId).addClass("active");
										$scope.displaySentiment(i, $scope.sentitId, 'permanent' );										
									}
								}
							}
						}
						$scope.permanentSentimentsSets.push(newobj);
					}
					if($routeParams.sentimentId != null && $routeParams.sentimentId != undefined && !$scope.sentimentFound && $scope.permSentimentCaurselTo < $rootScope.totalPermSentiemnts ){
						  $scope.permSentimentCaurselFrom = $scope.permSentimentCaurselTo;
						  $scope.permSentimentCaurselTo += $scope.sentimentCauroselMaxLength;
						  setPermanentSentimentsSets($scope.permSentimentCaurselFrom,$scope.permSentimentCaurselTo);
					}
					$("#carousel-sentiment-perma").carousel({interval: 5000, pause: "hover"});
					
				});
			}
			
			function setTemporarySentimentsSets (from,to){

				SentimentService.findByTypeTemporary(from,to).then(function(response) {
					$scope.temporarySentiments = response;
					var data = response;
					var sentimentLength = data.length;
					for (var i = 0; i < sentimentLength; i += 8) {
						var newobj = {};
						for(var j=i; j< (i+8); j++){
							if(data[j]){
								newobj[j]= data[j]; 
								
								if($routeParams.sentimentId != null && $routeParams.sentimentId != undefined){
									if(data[j].id == $routeParams.sentimentId ){
										$scope.sentimentFound = true;
										$scope.tab=1;
										$("#current"+$scope.sentitId).addClass("active");
										$scope.displaySentiment(i, $scope.sentitId, 'temporary' );										
									}
								}
							}
						}
						$scope.temporarySentimentsSets.push(newobj);
					}
					if($routeParams.sentimentId != null && $routeParams.sentimentId != undefined &&!$scope.sentimentFound &&  $scope.tempSentimentCaurselTo < $rootScope.totalTempSentiemnts ){
						  $scope.tempSentimentCaurselFrom = $scope.tempSentimentCaurselTo;
						  $scope.tempSentimentCaurselTo += $scope.sentimentCauroselMaxLength;
						  setTemporarySentimentsSets($scope.tempSentimentCaurselFrom,$scope.tempSentimentCaurselTo);
					}$("#carousel-sentiment").carousel({interval: 5000, pause: "hover"});
					
				});
			}
			
			
	/*===================================/BY SENTIMENT ID IN THE URL========================================*/	
			
			/*if($scope.isUserLoggedIn() == true){
				$
			}else{
			 $scope.loginNow();
			}*/	
	/*============================IMAGE MODAL ONCLICK EVENT OF SENTIMENT IMAGE=======================================*/		
			$scope.showImage=function(){
			$('#sentimentPic_Modal').appendTo("body").modal('show');
			};
			/*==========================URLIVE IMPLEMENTATION FOR NEWS PREVIEW==========================================*/
			/*	$('#news_img').urlive({
			 imageSize: 'small';
			 });*/
			$scope.pollSlider= function(inx){
				$(slide).carousel('inx');
			}
			$('#poll_cir').css('margin-left', "$('#p_15').outerWidth() / 2 + 'px'");
			$scope.stylePoll=function(pollId, inx, polls){
				debugger;
				for(var i=0; i<polls.length; i++){$('#p_1_'+polls[i].id).css('background','');$('#p_1_'+polls[i].id).css('transform','scale(1)');}
				$('#p_1_'+pollId).css('background', '#ef2d27'); $('#p_1_'+pollId).css('transform','scale(1.3)');
				
				$('#p_1_'+pollId).css('background','red');
				$('#p_1_'+pollId).attr('data-target','#carousel-poll');
				$('#p_1_'+pollId).attr('data-slide-to',inx);
			}
			
		/*	$scope.classPopup= function(attaId){
	            dropDownFixPosition($('#refImg_'+attaId),$('#popupSmilee_'+attaId));
	        };
	function dropDownFixPosition(button,dropdown){
		//alert(button.offset().top+"px");
	      var dropDownTop = button.offset().top + button.outerHeight();
	        dropdown.css('top', dropDownTop + "px");
	        dropdown.css('left', button.offset().left + "px");
	}*/
			
			$( ".toggle_txt ul li.last a" ).click(function() {
			$( ".collapse_sec" ).show();
			$( ".show_onclick" ).hide();

			$( ".amChartsPeriodSelector .amChartsInputField" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
		});
		//$('#search_drp').hide();
	/*	$('body').click(function(event) {
			//alert($('#search_drp').css('display'));  
			if(event.target.id!="arrw_bar" && event.target.id!="arrw"){
				if($('#search_drp').is(":visible")==true);
				{$('#search_drp').hide();}
			}else{
				if(event.target.id=="arrw_bar" || event.target.id=="arrw"){
					if($('#search_drp').is(":visible")==false)
					{$('#search_form').addClass('search');
						$('#search_drp').show();}else{$('#search_drp').hide();}
				}
			}
			
			
		
			});*/
		 
		
		$('#refPar_smiley').click(function(){
			//alert();
			//$('.popup_smiley').css("position","absolute");
			$('.popup_smiley').css("overflow", "visible");
			$('.popup_smiley').css("position", "fixed");
			$('.popup_smiley').css("top", "1000px");
			$('.popup_smiley').css("left", "350px");
			$('.popup_smiley').css("width", "inherit");
			$('.popup_smiley').show();
		});
	 
		}])
	.filter('trustAsResourceUrl', ['$sce', function($sce) {
		return function(val) {
			return $sce.trustAsResourceUrl(val);
		};



	}]).directive('callback', function() {
		  return function(scope, element, attrs) {        
			    setTimeout(function doWork(){
			      //jquery code and plugins

		    		if(attrs.sntmId == scope.sentitId){
						$(element).addClass('active');
					  }
		    	
				  
			    }, 0);        
			  };
			}).directive('tooltipCalback', function() {
  return function(scope, element, attrs) {        
    setTimeout(function doWork(){
      //jquery code and plugins
	  if(attrs.sntmId == scope.sentimentIdParam){
		$(element).tooltip();
	  }
    }, 0);        
  };
});

var pauseSlides = function(){
	$("#carousel-sentiment").carousel("pause");
	$("#carousel-sentiment-perma").carousel("pause");
	$("#carousel-related").carousel("pause");
	//$("#carousel-poll").carousel("pause");
};


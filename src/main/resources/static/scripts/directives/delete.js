'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('deleteModal', function () {
    return {
      templateUrl: 'templates/modal_delete.html',
	  restrict: 'E',
      link: function postLink(scope, element, attrs) {},
	  controller: ['$scope','$http','$rootScope','$location', function ($scope, $http, $rootScope,$location) {
		  
		  $scope.deleteId = "";
     	  $scope.loggedInUser = "";
		  $scope.commentType = ""
		  
		 $scope.closeDeletePopup = function(){
    		 $scope.deleteId = "";
    		 $scope.commentType = ""
    		 $('#modal_delete_opinion').modal("hide");
    		 $('#modal_delete_response').modal("hide");
    		 $('#modal_delete_comment').modal("hide");
    		 $('#modal_delete_not_owner').modal("hide");    		 
 		};
    	 $scope.confirmDeleteOpinion = function(id,ownerId) {
    		 $scope.deleteId = id;
    		 if($scope.isUserLoggedIn() == true){ 
    			 $scope.loggedInUser =	 $scope.getLoggedInUserId();
    			 if(ownerId == $scope.loggedInUser ){
        			 $('#modal_delete_opinion').modal("show");
        		 }else{
        			 $('#modal_delete_not_owner').modal("show");
        		 }
        		
    		 } 
				else{
					$scope.loginNow();
				}
    		  
    	 };
    	
    	 $scope.confirmDeleteResponse = function(id,ownerId) {
    		 $scope.deleteId = id;
    		 if($scope.isUserLoggedIn() == true){
    			 $scope.loggedInUser =	 $scope.getLoggedInUserId();
    			 if(ownerId == $scope.loggedInUser ){
        			 $('#modal_delete_response').modal("show");
        		 }else{
        			 $('#modal_delete_not_owner').modal("show");
        		 } 
    		 }
				else{
					$scope.loginNow();
				}
    	 };
    	 

    	 $scope.confirmDeleteComment = function(id,type,ownerId) {
    		 $scope.deleteId = id;
    		 $scope.commentType = type;
    		 if($scope.isUserLoggedIn() == true){ 
    			 $scope.loggedInUser =	 $scope.getLoggedInUserId();
					if(ownerId == $scope.loggedInUser){
						 $('#modal_delete_comment').modal("show");
					}/*else{
						 $('#modal_delete_not_owner').modal("show");
					}*/
				}
				else{
					$scope.loginNow();
				}
    	 };
    	 
    	 $scope.deleteOpinion = function() {								    		
    		  $rootScope.$emit("CallParentDeleteOpinion", [$scope.deleteId]);
    	 }
 	 
    	 $scope.deleteResponse = function() {								    		
   		  $rootScope.$emit("CallParentDeleteResponse", [$scope.deleteId]);
    	 }
    	 
    	 $scope.deleteComment = function() {								    		
      		  $rootScope.$emit("CallParentDeleteComment", [$scope.deleteId, $scope.commentType]);
       	 }
   	 
	  }]
    };
  });

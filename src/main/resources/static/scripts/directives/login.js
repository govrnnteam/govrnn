'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('loginModal', function () {
    return {
      templateUrl: 'templates/modal_login.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the myDirective directive');
        // $('.animation_text input').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });

        // $('.animation_text textarea').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });
        // $('.animation_text select').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });


        // $('.animation_text.dob input').on('click', function(){
          // $(this).parent('.animation_text').addClass('has_txt');
        // });

        // var inputs = $('.animation_text input').not(':submit');

        // inputs.on('input', function() {
          // $(inputs[inputs.index(this)]).parent().toggleClass('has_txt', this.value > '');
        // });
        // inputs[0].focus();
		
		
		
        // $('.animation_text input').on('change', function(){
          // $(this).parent('.animation_text').addClass('has_txt');
        // });
		
      },
      controller: ['$scope','$http','$rootScope','$localStorage','$cookies','$window', '$location', '$route', function ($scope, $http, $rootScope, $localStorage, $cookies, $window, $location, $route) {

        $scope.user = {};
        $scope.hasError = false;
		$scope.$storage = $localStorage;
		$scope.GTopNoti = '';
		
		
        /* reset logged in user password*/
        $scope.authenticate = function(form) {
          if (form.$valid) {
            var loginData = $.param({
              username: $scope.user.name,
              password: $scope.user.password
            });
			// console.log(loginData);
            $http({
              method: 'POST',
              url: $rootScope.liveHost+"/user/authenticateUser",
			  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			  // headers: {'Content-Type': 'application/form-data'},
            //  withCredentials: true,
              data: loginData
            }).then(function successCallback(response) {
            	if(response.data.success === true){				
				  // console.log('sucess : ', response.data);
            		
            		if(response.data.messages[0].status == 'Deleted'){
            			alert('User Does not exist');
            		}
            		else{
            			  angular.forEach(response.data.messages,function(value,key){
        					  $('#login_pop').modal("hide");
        					  // console.log('value : ', value);
        					  $scope.GTopNoti = value.id;
        					  //$cookies.putObject('userData',value);			  
        					
        					  
        					  $cookies.putObject('userData', value);
        					  $cookies.putObject('userID', value.id);

        						localStorage.setItem('AuthData', JSON.stringify(value));
        					  
        					  $scope.$storage.sessionData = {};
        					  $scope.$storage.sessionData.isLoggedIn = true;
        					  $scope.$storage.sessionData.user_name = value.name;
        					  $scope.$storage.sessionData.user_id = value.id;
        					  
        					  $http.get('/user/downloadUserImage?userId='+value.id+'&imageType=userimage&imageSize=standard').then(function(response) {
        							if (response != null) {
        								var userImgval=response.data;
        								//alert(JSOG.stringify(response));
        								if(!response.data){//$('#cover_img').attr('src','images/personality.png');
        									
        								}
        								else{
        									$scope.$storage.sessionData.user_image = userImgval;
        									//$('.banner_perso').css('background-image','url(' +coverImgval+ ')');
        								}
        					}
        					 });        					 
        					
        					 // $scope.$storage.sessionData.user_image = value.userImages[0].image;
        					  $scope.user = {};
        					  $scope.userData = response.data.messages;

        					 // $cookies.putObject('user_obj', $scope.userData);
        					  
        					  // console.log("userData :" , JSON.parse(localStorage.getItem('AuthData')));

        					  $scope.callEvent();
        					  $scope.generalcount();
        					  
        					  // $location.href = "/";
        				  });
        				  $route.reload();
            		}
            		
				
				  // console.log("success login");
				}else{
				  $scope.hasError = true;	
				  $scope.user = {};
				  form.$setPristine();
				  // console.log("succes:error login");	
				}
            }, function errorCallback(response) {
              $scope.hasError = true;
              $scope.user = {};
			  form.$setPristine();
              // console.log("error login");
              // console.log(response);
            });

          }
        };
		
        $scope.viewNofication = function (){
        	// console.log('view');
        	$cookies.putObject('userData', value);
			  $cookies.putObject('userID', value.id);
        	$location.path('/viewAllOpinionNotifications');
        	//$location.href = "/viewAllOpinionNotifications";
        };
        
        $scope.generalNofication = function (){
        	// console.log('view');
        	$location.path('/viewGeneralNotification');
        	//$location.href = "/viewAllOpinionNotifications";
        };
        $scope.AllOpinionNotification=function(){
        	$location.path('/viewAllOpinionNotifications');
        	
        };
		$scope.isUserLoggedIn = function () {
		  if($scope.$storage.sessionData !== undefined && $scope.$storage.sessionData.isLoggedIn === true){
			$scope.loginLink = " "+$scope.$storage.sessionData.user_name+" | Logout";
			return true;
		  }else {
			return false;
		  }
		};
		
		$scope.getLoggedInUserId = function () {
		  if($scope.$storage.sessionData !== undefined){
			return $scope.$storage.sessionData.user_id;
		  }else {
			return null;
		  }
		};
		
		$scope.getUserImage = function () {
		  if($scope.$storage.sessionData !== undefined){
			return "data:image/jpg;base64,"+$scope.$storage.sessionData.user_image;
		  }else {
			return "/images/default-user.jpg";
		  }
		};
		
		$scope.loginNow = function(){
			$('#login_pop').modal("show");
			var i=0;
			$('.animation_text input').each(function(e){
				
				if($(this).val().length > 0){
					if(i == 0){
						console.log("i is 0");
						$('#login_user').focus();
					}
					$(this).parent('.animation_text').addClass('has_txt');
					i++;
				}
			});
			
		}

		$scope.logout = function () {
			$scope.notificationCount = '';
			$scope.TopnotificationCount = [];
			delete $scope.$storage.sessionData;
			$scope.GTopNoti = '';						
			  $cookies.putObject('userID', '');
			 $cookies.put('userData','');
			
			localStorage.removeItem("AuthData");

			$scope.notificationCount = '';
			 $scope.generalNotificationCount = '';
			
			$location.url("/");
		};
		
		
		$scope.callEvent = function(){
			// console.log("callEvent  $scope.GTopNoti ::"+$scope.GTopNoti);
			$http({
	            method: 'GET',
	            url: $rootScope.liveHost+"/notification/getUnreadNotificationsCount",
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				  params: {userId: $cookies.getObject('userID'),notificationType:'Opinion'}
	          }).then(function successCallback(response) {
	        	  if(response.data.success === true){				
	        		  angular.forEach(response.data.messages,function(val,k){
	        			  $scope.notificationCount = val;  
	        			  // console.log("notificationCount -- 1",$scope.notificationCount);
	        		  });
					}else{
					  $scope.hasError = true;
					  $scope.notificationCount = '';
					  $scope.user = {};
					  form.$setPristine();
					  // console.log("succes:error login");	
					}
	          }, function errorCallback(response) {
	        	  $scope.notificationCount = '';
	            $scope.hasError = true;
	            $scope.user = {};
				  form.$setPristine();
	            // console.log("error login");
	            // console.log(response);
	          });
		};
		
		
		
		// start 
		$scope.generalcount = function(){
			// console.log("$scope.GTopNoti ::"+$scope.GTopNoti);
			$http({
	            method: 'GET',
	            url: $rootScope.liveHost+"/notification/getUnreadNotificationsCount",
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				  params: {userId: $cookies.getObject('userID'),notificationType:'General'}
	          }).then(function successCallback(response) {
	        	  if(response.data.success === true){				
	        		  angular.forEach(response.data.messages,function(val,k){
	        			  $scope.generalNotificationCount = val;  
	        			  // console.log("generalNotificationCount -- 2",$scope.generalNotificationCount);

	        		  });
					}else{
						$scope.generalNotificationCount = '';
					  $scope.hasError = true;
					  $scope.user = {};
					  form.$setPristine();
					  // console.log("succes:error login");	
					}
	          }, function errorCallback(response) {
	            $scope.hasError = true;
	           
	            $scope.user = {};
				  form.$setPristine();
	            // console.log("error login");
	            // console.log(response);
	          });
		};
		
		$scope.changeStatus = function (data) {
			// console.log('OK CHECKED--',data);
			
			$http({
	            method: 'GET',
	            url: $rootScope.liveHost+"/notification/updateNotificationreadStatus",
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				  params: {notificationMongoId:data.notificationId}
	          }).then(function successCallback(response) {
	        	  if(response.data.success === true){				
	        		  
	        		  // console.log('getNoti : ', JSON.stringify(response.data.messages));
	        		  $scope.TopnotificationCount = response.data.messages;
	        		  
	        		  
	        		  $scope.callEvent();
	        		  $scope.generalcount();
					}else{
					  $scope.hasError = true;
					  $scope.user = {};
					  form.$setPristine();
					  // console.log("succes:error login");	
					}
	          }, function errorCallback(response) {
	            $scope.hasError = true;
	            $scope.user = {};
				  form.$setPristine();
	            // console.log("error login");
	            // console.log(response);
	          });
			
		};
		
		$scope.getTopNotifications = function () {
			
			var gettoID = $cookies.getObject('userID');
			// console.log("temp gettoID : ", gettoID);
			
			$scope.TopNotificationsLoginflag =false;

			if(gettoID == null ||gettoID == undefined||gettoID == '' ){
				$scope.TopNotificationsLoginflag =false;

			}else{
				$scope.TopNotificationsLoginflag =true;

			}
			
		
//			if( $scope.$storage.sessionData.isLoggedIn = true){
//				$scope.TopNotificationsLoginflag =true;
//				}
			
			$http({
	            method: 'GET',
	            url: $rootScope.liveHost+"/notification/getOpinionNotifications",
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				  params: {userId: $cookies.getObject('userID'),componentType:'Opinion'}
	          }).then(function successCallback(response) {
	        	  if(response.data.success === true){				
	        		  // console.log('getNoti : ', JSON.stringify(response.data.messages));
	        		  $scope.TopnotificationCount = response.data.messages;
					}else{
					  $scope.hasError = true;
					  $scope.user = {};
					  form.$setPristine();
					  // console.log("succes:error login");	
					}
	          }, function errorCallback(response) {
	            $scope.hasError = true;
	            $scope.user = {};
				  form.$setPristine();
	            // console.log("error login");
	            // console.log(response);
	          });
		};
		
		
		$scope.getGeneralNotifications = function () {
			
			var gettoID = $cookies.getObject('userID');

			$scope.GeneralNotificationsLoginflag =false;
			
			if(gettoID == null ||gettoID == undefined||gettoID == '' ){
				$scope.GeneralNotificationsLoginflag =false;
			}else{
				$scope.GeneralNotificationsLoginflag =true;
			}
			
//			if($scope.GTopNoti.length = 0){
//				$scope.GeneralNotificationsLoginflag =false;
//			}
//			
			
//			if( $scope.$storage.sessionData.isLoggedIn = true){
//				$scope.GeneralNotificationsLoginflag =true;
//				}
			
			$http({
	            method: 'GET',
	            url: $rootScope.liveHost+"/notification/getGeneralNotifications",
				  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				  params: {userId: $cookies.getObject('userID'),componentType:'General'}
	          }).then(function successCallback(response) {
	        	  if(response.data.success === true){				
	        		  // console.log('getNoti : ', JSON.stringify(response.data.messages));
	        		  $scope.GeneralnotificationCount = response.data.messages;
//	        		  $scope.GeneralnotificationCount ='';
//	        		  $scope.jsnGeneralNotifiacation = response.data.messages;
//	        		  if($scope.jsnGeneralNotifiacation.length>10){
//	        			  for(var j= 0;j<$scope.jsnGeneralNotifiacation;j++){
//	        				  $scope.GeneralnotificationCount.push()
//	        			  }
//	        		  }
					}else{
					  $scope.hasError = true;
					  $scope.user = {};
					  form.$setPristine();
					  // console.log("succes:error login");	
					}
	          }, function errorCallback(response) {
	            $scope.hasError = true;
	            $scope.user = {};
				  form.$setPristine();
	            // console.log("error login");
	            // console.log(response);
	          });
		};
		
		$scope.searchValue = '';
		$scope.SearchValueFunction = function (val){
			var srchfld = $("#searchInputFld");
			switch(val){
			case 1:
				$scope.searchValue = 'ALL';
				srchfld.attr("placeholder","Search Opinion, People ...please enter at least 3 characters");
				break;
			case 2:
				$scope.searchValue = 'People';
				srchfld.attr("placeholder","Search People ...please enter at least 3 characters");
				break;
			case 3:
				$scope.searchValue = 'Opinion';
				srchfld.attr("placeholder","Search Opinion ...please enter at least 3 characters");
				break;
			case 4:
				$scope.searchValue = 'Sentiment';
				break;
			}
		}
		
		$scope.searchFunction = function (){
			if($scope.searchValues.length>2){
				$location.path('/searchuser').search({componentType:$scope.searchValue , searchKeyword:$scope.searchValues})
			}			
		}
	
        $scope.showForgotPwdPopup = function (form) {
          $scope.user = {};
		  form.$setPristine();
		  $scope.forgotPwdStatus = false;
  		  $scope.forgotPwdDeletedStatus = false;

          $('#login_pop').modal("hide");
          $('#forgotpwd_pop').modal("show");
        };
        $scope.showForgotUserPopup = function (form) {
          $scope.user = {};
		  form.$setPristine();
		  $scope.forgotUserStatus = false;
		  $scope.forgotUserDeletedStatus = false;
	        
          $('#login_pop').modal("hide");
          $('#forgotuser_pop').modal("show");
        };
		$scope.showSignupPopup = function (form) {
          $scope.user = {};
		  form.$setPristine();
          $('#login_pop').modal("hide");
          $('#sign_pop').modal("show");
        };
		$scope.closeLoginPopup = function(form){
			$scope.user = {};
			form.$setPristine();
			$('#login_pop').modal("hide");
		};
		

		if($cookies.getObject('userID') == null || $cookies.getObject('userID') == undefined || $cookies.getObject('userID') == ''){			
			$scope.logout();
		}else{
			$scope.userData =[];
			$scope.userData[0] = JSON.parse(localStorage.getItem('AuthData'));

		$scope.callEvent();
		$scope.generalcount();
		}
      }]
    };
  }).directive('initFocus', function() {
	return function(scope, element, attrs) {
		setTimeout(function doWork(){
			//jquery code and plugins
			//element.focus();
			console.log("Calling focus");
			$('#login_user').focus();
		}, 0);
	};
});
